#define RUN_ON_ANDROID         // delete this to compile this file without all the android-stuff... (i need to do that for checking everything with valgrind. not on the phone but on the laptop)
#define DO_OUTPUT

#ifdef RUN_ON_ANDROID
#include <jni.h>
#include <android/log.h>    //logcat
#endif

#include <cstring>
#include <clocale>
#include <cerrno>
#include <sstream>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <map>
#include <thread>
#include <mutex>
#include <semaphore.h>
#include <math.h>
//#include <ctime>

//#include <sys/types.h>      //
#include <sys/socket.h>     //        = needed for the socket and server stuff
#include <netinet/in.h>     //
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/signal.h>

#include <netdb.h>
//#include "tcpconnector.h"

#include <sys/time.h>           // e.g. time since unix-birth

#include <unistd.h>         //usleep
#include <strings.h>
#include <string.h>

#include <stdlib.h>     /* srand, rand */

//#define MIN_TIME_BETWEEN_PINGS 1500             //milliseconds
#define MIN_TIME_BETWEEN_PINGS 4000             //milliseconds
#define INTERVALL_CHECK_OFFLINE_CLIENTS 5000    //milliseconds
#define CLIENT_TIMEOUT 10000                    // milliseonds
#define SOCKET_READ_TIMEOUT_SEC 3               // seconds
#define READ_MESSAGE_BUFFER 128
#define PORT 4223

//using namespace std;

class ConnectionManager;
class Client;
class MessageGenerator;
class Bateria;
class Log;
class GlobalLog;
//class Effect;
class WaterDrop;

//string _appFolder;

ConnectionManager* _connectionManager = NULL;
MessageGenerator* _messageGenerator = NULL;
Bateria* _bateria = NULL;

GlobalLog* _globalLog = NULL;


typedef enum{
    unknown,
    mestre,
    caixa,
    tamborim,
    repique,
    chocalho,
    primera,
    segunda,
    tetiera,
    agogo,
    nullpointer
}Instrument;

std::vector< std::pair< Client*, Instrument > > _clientSnapshotForInformationCollection;
std::vector< WaterDrop* > _effectSnapshotForInformationCollection;
std::mutex _snapshotMutex;

class ColorSet{
    public:
        ColorSet(){};

        ColorSet( int in_red, int in_green, int in_blue, int in_uv ){
            red = in_red;
            green = in_green;
            blue = in_blue;
            uv = in_uv;
        };

        ColorSet( bool in_randomRedValue, bool in_randomGreenValue, bool in_randomBlueValue, bool in_randomUvValue ){
            red = -1;
            green = -1;
            blue = -1;
            uv = -1;
            randomRed = in_randomRedValue;
            randomGreen = in_randomGreenValue;
            randomBlue = in_randomBlueValue;
            randomUV = in_randomUvValue;
        }

        ~ColorSet(){};

        int red = 0;
        int green = 0;
        int blue = 0;
        int uv = 0;     // i want to make clients with an extra UV-LED :-)
        bool randomRed = false;
        bool randomGreen = false;
        bool randomBlue = false;
        bool randomUV = false;
};

template <typename T> std::string to_string( T value ){  // from https://stackoverflow.com/questions/22774009/android-ndk-stdto-string-support
    std::ostringstream os ;
    os << value ;
    return os.str();
}

typedef struct{
    std::string type = "";
    std::string content = "";
}Command;

class Position{
    public:
        Position( int in_row, int in_colomn ){
            row = in_row;
            colomn = in_colomn;
        }
        int row = -1;
        int colomn = -1;
};

std::string getTimestampWithDateAndTime(){
    time_t currentTime = time(0);
    struct tm tstruct;
    char output[80];
    tstruct = *localtime( &currentTime );
    strftime( output, sizeof(output), "%b %d %Y %X", &tstruct );
    return output;
}

std::string getTimeOfDay(){
    time_t currentTime = time(0);
    struct tm tstruct;
    char output[80];
    tstruct = *localtime( &currentTime );
    strftime( output, sizeof(output), "%X", &tstruct );
    return output;
}

unsigned long int getUnixTime(){
    struct timeval tp;
    gettimeofday(&tp, NULL);
    unsigned long int output = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    return output;
}

std::string extractChipID( std::string in_identifyString ){
    bool chipID_found = false;
    std::string output = "";
    char symbol;
    for( size_t index = 0; index < in_identifyString.length() && chipID_found == false; index++ ){
        symbol = in_identifyString.at(index);
        if( symbol == '_' && chipID_found == false ){
            chipID_found = true;
        }
        if( chipID_found == false ){
            output += symbol;
        }
    }
    return output;
}

bool stringCompare( std::string in_string0, std::string in_string1 ){
    bool output = true;
    int lengthString0 = in_string0.length();
    if( lengthString0 == in_string1.length() ){
        for( size_t index = 0; index < lengthString0 && output == true; index++ ){
            char symbol0 = in_string0.at(index);
            char symbol1 = in_string1.at(index);
            if( symbol0 != symbol1 ){
                output = false;
            }
        }
    }else{
        output = false;
    }
    return output;
}

#ifdef RUN_ON_ANDROID
std::string jstring2string(JNIEnv *env, jstring in_javaString) {  // from: https://stackoverflow.com/questions/41820039/jstringjni-to-stdstringc-with-utf8-characters
    if(!in_javaString){
        return "";
    }
    const jclass stringClass = env->GetObjectClass(in_javaString);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(in_javaString, getBytes, env->NewStringUTF("UTF-8"));

    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte* pBytes = env->GetByteArrayElements(stringJbytes, NULL);

    std::string ret = std::string((char *)pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}
#endif

class GlobalLog{

    public:
        GlobalLog( std::string in_appFolder ){
            _rootPath = in_appFolder;
            newFile();
        };

        ~GlobalLog(){};


        void newFile(){
            #ifdef RUN_ON_ANDROID
            //__android_log_print( ANDROID_LOG_VERBOSE, "_debug", "entering newFile()", 1 );
            __android_log_print( ANDROID_LOG_VERBOSE, "_debug", "entering newFile()" );
            #else
            cout << "entering newFile()" << endl;
            #endif

            _fileMutex.lock();
            std::string temp = to_string( _rootPath + getTimestampWithDateAndTime() + ".txt" );
            if( _path_currentFile != NULL ){
                delete[] _path_currentFile;
            }
            _path_currentFile = new char[ temp.length() + 1 ];
            strcpy( _path_currentFile, temp.c_str() );
            _fileMutex.unlock();
        }

        void addLine( std::string in_line ){
            _fileMutex.lock();

            #ifdef RUN_ON_ANDROID
            //__android_log_print( ANDROID_LOG_VERBOSE, "_debug", "entering addLine()", 1 );
            #else
            //std::cout << "entering addLine()" << std::endl;
            #endif

            if( _path_currentFile != NULL ){
                FILE* file = NULL;
                file = fopen( _path_currentFile, "a+" );
                fputs( ( getTimestampWithDateAndTime() + ": " + in_line + "\n" ).c_str(), file );
                fclose( file );
            }else{
                #ifdef RUN_ON_ANDROID
                //__android_log_print( ANDROID_LOG_VERBOSE, "_debug", "_path_currentFile == NULL", 1 );
                __android_log_print( ANDROID_LOG_VERBOSE, "_debug", "_path_currentFile == NULL" );
                #else
                std::cout << "_path_currentFile == NULL" << std::endl;
                #endif
            }
            _fileMutex.unlock();
        }

    private:
        std::string _rootPath = "";
        char* _path_currentFile = NULL;
        std::mutex _fileMutex;
};

void print( std::string in_string ){
    #ifdef DO_OUTPUT
    #ifdef RUN_ON_ANDROID
    __android_log_print( ANDROID_LOG_VERBOSE, "_debug", in_string.c_str(), 1 );
    if( _globalLog != NULL ){
        _globalLog->addLine(in_string);
    }
    #else
    cout << in_string << endl;
    if( _globalLog != NULL ){
        _globalLog->addLine(in_string);
    }
    #endif
    #endif
}

void handler(int in_s) {
    print( "Caught SIGPIPE. in_s: " + to_string(in_s) );
}


Instrument getInstrumentOfString( std::string in_instrument ){
    Instrument output = unknown;
    if( stringCompare( in_instrument, "unknown" ) ){
        output = unknown;
    }else if( stringCompare( in_instrument, "mestre" ) ){
        output = mestre;
    }else if( stringCompare( in_instrument, "caixa" ) ){
        output = caixa;
    }else if( stringCompare( in_instrument, "tamborim" ) ){
        output = tamborim;
    }else if( stringCompare( in_instrument, "repique" ) ){
        output = repique;
    }else if( stringCompare( in_instrument, "chocalho" ) ){
        output = chocalho;
    }else if( stringCompare( in_instrument, "primera" ) ){
        output = primera;
    }else if( stringCompare( in_instrument, "segunda" ) ){
        output = segunda;
    }else if( stringCompare( in_instrument, "tetiera" ) ){
        output = tetiera;
    }else if( stringCompare( in_instrument, "agogo" ) ){
        output = agogo;
    }else{
        print("getInstrumentOfString(): invalid Instrument-string");
    }
    return output;
}

std::vector< Command > extractCommand( std::string in_message, std::string* in_remainString ){
    std::vector<Command> output;
    std::string message = *in_remainString + in_message;
    *in_remainString = "";
    bool beginsWithStartSymbol = true;

    //debug
    //if( in_message[0] == '@' && in_message[1] != 'p' && in_message[1] != 's' && in_message[1] != 'i' ){
    //    print("break");
    //}
    //


    if( message.at(0) != '@' ){
        print("fail! combination of remianString and in_message doesn't begin with an '@'");
        beginsWithStartSymbol = false;
    }

    if( beginsWithStartSymbol ){

        int numberOfEndSymbols = 0;
        for(int index = 0; index < message.length(); index++) {
            if( message.at(index) == '#' ){
                numberOfEndSymbols++;
            }
        }

        for( int messageIndex = 0; messageIndex < numberOfEndSymbols; messageIndex++ ){
            bool commandTypeComplete = false;
            char symbol = ' ';
            Command singleCommand;
            for(int index = 0; symbol != '#'; index++){
                symbol = message.at(0);
                message = message.substr(1);
                if(symbol == ';'){
                    commandTypeComplete = true;
                    index++;
                    symbol = message.at(0);
                    message = message.substr(1);
                    //print("symbol: " + symbol);
                }
                if(commandTypeComplete == false){
                    if (symbol != '@') {
                        singleCommand.type += symbol;
                    }
                }else{
                    if (symbol != '#') {
                        singleCommand.content += symbol;
                    }
                }
            }
            output.push_back( singleCommand );
        }
        *in_remainString = message;
    }
    return output;
}

unsigned int getCurrentTime(){
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int output = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    return output;
}

class MessageGenerator{

    public:
        MessageGenerator(){
            WHITE.red = 255;
            WHITE.green = 255;
            WHITE.blue = 255;
            WHITE.uv = 0;

            OFF.red = 0;
            OFF.green = 0;
            OFF.blue = 0;
            OFF.uv = 0;

            RED.red = 255;
            RED.green = 0;
            RED.blue = 0;
            RED.uv = 0;

            DARK_GREEN.red = 0;
            DARK_GREEN.green = 32;
            DARK_GREEN.blue = 0;
            DARK_GREEN.uv = 0;
        };

        ~MessageGenerator(){};

        std::string blinkThreeTimesInColor(ColorSet in_color){
            std::string output = "@fadeSeries4AllPixels;";
            addFadeToFadeSeriesString(&output, in_color, 1, 300);
            addFadeToFadeSeriesString(&output, OFF, 1, 300);
            addFadeToFadeSeriesString(&output, in_color, 1, 300);
            addFadeToFadeSeriesString(&output, OFF, 1, 300);
            addFadeToFadeSeriesString(&output, in_color, 1, 300);
            addFadeToFadeSeriesString(&output, OFF, 1, 2000);   // the 2000 will not have any effect as it is the hold-time in last state of series
            output += "#";
            return output;
        };

        std::string getFadeToColorInSteps( ColorSet in_color, int in_steps ){
            std::string output = "@fadeSeries4AllPixels;";
            addFadeToFadeSeriesString( &output, in_color, in_steps, 0 );
            output += "#";
            return output;
        }

        std::string getFadeWithAttackAndReleaseAndBackToOff( ColorSet in_color, int in_stepsAttack, int in_stepsRelease ){
            std::string output = "@fadeSeries4AllPixels;";
            addFadeToFadeSeriesString( &output, in_color, in_stepsAttack, 0 );
            addFadeToFadeSeriesString( &output, OFF, in_stepsRelease, 0 );
            output += "#";
            return output;
        }

        std::string getFadeToColorInOneStepAndBackToOff( ColorSet in_color, int in_holdMilliseconds  ){
            std::string output = "@fadeSeries4AllPixels;";
            addFadeToFadeSeriesString( &output, in_color, 1, in_holdMilliseconds );
            addFadeToFadeSeriesString( &output, OFF, 1, 0 );
            output += "#";
            return output;
        }

        std::string sleepForSeconds( int in_seconds ){
            std::string output = "@sleepSeconds;";
            output += to_string(in_seconds);
            output += "#";
            return output;
        }

        std::string shouldMakeSignals( bool in_shouldMakeSignals ){
            std::string output = "@makeSignals;";
            if( in_shouldMakeSignals ){
                output += "true#";
            }else{
                output += "false#";
            }
            return output;
        }

        std::string setClientToWifiForcedSleepWithSeconds(int in_seconds){
            std::string output = "@wifiForcedSleepLoopOn;";
            output += to_string( in_seconds );
            output += "#";
            return output;
        }

        std::string setClientToDeepSleepWithSeconds( int in_seconds ){
            std::string output = "@deepSleepInLoopOn;";
            output += to_string( in_seconds );
            output += "#";
            return output;
        }

        std::string reset(){
            std::string output = "@reset;#";
            return output;
        }

        std::string getSetTriggerThreshold( int in_triggerThreshold ){
            return "@setTriggerThreshold;" + to_string( in_triggerThreshold ) + "#";
        }

        ColorSet WHITE;
        ColorSet OFF;
        ColorSet RED;
        ColorSet DARK_GREEN; // because michi is a memme

    private:
        std::string serializeColorSet( ColorSet in_colorSet ) {
            std::string output = "";
            if( in_colorSet.randomRed == false ){
                output += to_string(in_colorSet.red);
            }else{
                output += "r";
            }
            output += ",";
            if( in_colorSet.randomGreen == false ){
                output += to_string(in_colorSet.green);
            }else{
                output += "r";
            }
            output += ",";
            if( in_colorSet.randomBlue == false ){
                output += to_string(in_colorSet.blue);
            }else{
                output += "r";
            }
            output += ",";
            if( in_colorSet.randomUV == false ){
                output += to_string(in_colorSet.uv);
            }else{
                output += "r";
            }
            return output;
        };

        void addFadeToFadeSeriesString( std::string* in_eventString, ColorSet in_color, int in_steps, int in_holdMilliseconds ){
            // a event in a sequence of fades, which will transmitted in this form: 255,0,0,0-1-300_
            // this is an example for a blinkin in red (we switch to this color in 1 step, and hold that for 300 milliseconds
            // but thats only half of it...
            // after that it only makes sense to change to another color, like 0,0,0,0 (led is off)
            // so this two fades (packed in one event) look like this: 255,0,0,0-1-300_0,0,0,0-1-1337_
            // the last number (1337) does not matter in this case
            *in_eventString += serializeColorSet( in_color );
            *in_eventString += '-';
            *in_eventString += to_string( in_steps );
            *in_eventString += '-';
            *in_eventString += to_string( in_holdMilliseconds );
            *in_eventString += '_';
        };

};

class Bateria{

    public:
        Bateria(int in_rows, int in_colomns){
            _rows = in_rows;
            _colomns = in_colomns;
            _mutex_bateriaVector.lock();
            for( int rowIndex = 0; rowIndex < in_rows; rowIndex++ ){
                std::vector< Client* > temp_row;
                for( int colomnIndex = 0; colomnIndex < in_colomns; colomnIndex++ ){
                    Client* noClient = NULL;
                    temp_row.push_back( noClient );
                }
                _bateriaVector.push_back( temp_row );
            }
            _mutex_bateriaVector.unlock();

            std::vector< Client* > emptyClientVector;
            std::pair< Instrument, std::vector<Client*> > temp_pair;
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(mestre, emptyClientVector) );
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(caixa, emptyClientVector) );
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(tamborim, emptyClientVector) );
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(repique, emptyClientVector) );
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(chocalho, emptyClientVector) );
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(primera, emptyClientVector) );
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(segunda, emptyClientVector) );
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(tetiera, emptyClientVector) );
            _sections.insert( std::pair<Instrument, std::vector<Client*>>(agogo, emptyClientVector) );

        };

        ~Bateria(){};

        Instrument getInstrumentOfClientWithChipID( std::string in_chipID );

        bool getLearnMode(){
            return _learnMode;
        }

        void restartAllActiveEffects();

        void learnClient(Client* in_client){
            _learnModeMutex.lock();
            if( _learnMode == true ){ // checking it here again, to avoid race-conditions..
                *_learnClient = in_client;

                // now we stop and re-start all effects, to get all threads starting correctly
                print("restarting all Effects");
                restartAllActiveEffects();

                _learnMode = false;
                _learnClient = NULL;
            }else{
                print("Bateria::learnClient(): _learnMode == false!!!");
            }
            _someClientToInstrumentAssignmentHasChanged = true;


            _learnModeMutex.unlock();
        }

        void assignClientToPosition( Client* in_client, Position in_position ){
            _mutex_bateriaVector.lock();
            _bateriaVector[in_position.row][in_position.colomn] = in_client;

            // now we stop and re-start all effects, to get all threads starting correctly
            restartAllActiveEffects();
            //startAllEffects();


            _mutex_bateriaVector.unlock();
            _someClientToInstrumentAssignmentHasChanged = true;
        }


        bool learnPosition( Position in_position ){     //returns true, if Client on position was null, returns false, if client on this position is already set. it will be overwritten anyways
            _learnModeMutex.lock();
            _mutex_bateriaVector.lock();
            bool output = false;
            Client* clientOnRequestedPosition = _bateriaVector[in_position.row][in_position.colomn];
            if( clientOnRequestedPosition == NULL ){
                output = true;
            }else{
                output = false;
            }
            _learnClient = &( _bateriaVector[in_position.row][in_position.colomn] );
            _learnMode = true;

            _mutex_bateriaVector.unlock();
            _learnModeMutex.unlock();
            return output;
        }

        void clearLearnPosition(){
            _learnClient = NULL;
            _learnMode = false;
        }

        std::string getInstrumentOnPosition( Position in_position );

        bool setInstrumentOnPosition( Position in_position, Instrument in_instrument );

        void addClientOnPositionToMarkedForAction( Position in_position ){
            _mutex_bateriaVector.lock();
            _markedForAction.push_back( _bateriaVector[in_position.row][in_position.colomn] );
            _mutex_bateriaVector.unlock();
        }

        void removeClientOnPositionFromMarkedForAction( Position in_position ){
            //print("entering Bateria::removeClientOnPositionFromMarkedForAction() ... in_position.row: " + to_string(in_position.row) + " ... in_position.colomn: " + to_string(in_position.colomn));
            _mutex_bateriaVector.lock();
            Client* clientOnThisPosition = _bateriaVector[in_position.row][in_position.colomn];
            _mutex_bateriaVector.unlock();
            bool wasDeleted = false;
            int indexOfClientToErase = -1;
            for( size_t index = 0; index < _markedForAction.size(); index++ ){
                if( _markedForAction[index] == clientOnThisPosition ){
                    indexOfClientToErase = index;
                }
            }
            if( indexOfClientToErase != -1 ){
                _markedForAction.erase( _markedForAction.begin() + indexOfClientToErase );
                //print( "deleted Client on position " + to_string( indexOfClientToErase ) );
                wasDeleted = true;
            }
            if(wasDeleted == false){
                print("Bateria::removeClientOnPositionFromMarkedForAction(): nothing found to delete... strange...");
            }else{
                //print("Bateria::removeClientOnPositionFromMarkedForAction(): successfully removed Client from _markedForAction");
            }
            //print("leaving Bateria::removeClientOnPositionFromMarkedForAction()");
        }

        int getRows(){
            int output = _rows;
            return output;
        }

        int getColomns(){
            int output = _colomns;
            return output;
        }

        void markSectionForAction( Instrument in_instrument ){
            _markedForAction.clear();       //delete old selection
            for( size_t index = 0; index < _sections[in_instrument].size(); index++ ){
                _markedForAction.push_back( _sections[in_instrument][index] );
            }
        }

        bool setMarkedClientsToColor( ColorSet in_colorSet );

        bool getSomeClientToInstrumentAssignmentHasChanged(){
            bool output = _someClientToInstrumentAssignmentHasChanged;
            _someClientToInstrumentAssignmentHasChanged = false;
            return output;
        }

        void sendFadeSeriesToClientOnPosition( Position in_position, std::string in_fadeSeries );

        bool getIsMarked( int in_row, int in_colomn ){
            bool output = false;
            _mutex_bateriaVector.lock();
            Client* clientOnPosition = _bateriaVector[in_row][in_colomn];
            _mutex_bateriaVector.unlock();
            for( size_t index = 0; index < _markedForAction.size() && output == false; index++ ){
                if( clientOnPosition == _markedForAction[index] ){
                    output = true;
                }
            }
            return output;
        }

        bool setInstrumentOfChipID( std::string in_chipID, Instrument in_instrument );

        bool setWaterDropEffectOnPosition( Position in_position, ColorSet in_color, int in_triggerThreshold, int in_speed, bool in_stayOnColor, int in_attack, int in_release );

        bool editWaterDropEffectOnPosition( Position in_position, ColorSet in_color, int in_speed, bool in_stayOnColor, int in_attack, int in_release );

        bool setWaterDropEffectOnChip( std::string in_chipID, ColorSet in_color, int in_triggerThreshold, int in_speed, bool in_stayOnColor, int in_attack, int in_release );

        Client* getPointerOfClientOnPosition( int in_row, int in_colomn ){
            return _bateriaVector[in_row][in_colomn];
        }

        int getNumberOfRows(){
            return _rows;
        }

        int getNumberOfColomns(){
            return _colomns;
        }

        Client** getPointerToClientPointerOnPosition( int in_row, int in_colomn ){
            return &( _bateria->_bateriaVector[in_row][in_colomn] );
        }

        bool getHasActiveEffect( int in_row, int in_colomn );

        bool getHasEffect( int in_row, int in_colomn );

        bool setEffectOnPositionToState( int in_row, int in_colomn, bool in_state );

        int getRedValueOfEffectOnPosition( int in_row, int in_colomn );

        int getGreenValueOfEffectOnPosition( int in_row, int in_colomn );

        int getBlueValueOfEffectOnPosition( int in_row, int in_colomn );

        int getUVValueOfEffectOnPosition( int in_row, int in_colomn );

        bool getEffectHasRandomColorOnPosition( int in_row, int in_colomn );

        int getEffectSpeedOnPosition( int in_row, int in_colomn );

        bool getEffectStayOnColorOnPosition( int in_row, int in_colomn );

        int getEffectAttackOnPosition( int in_row, int in_colomn );

        int getEffectReleaseOnPosition( int in_row, int in_colomn );

        void setLEDchainLength( int in_newChainLength, std::string in_chipID );

        int setTriggerThresholdOnEffectOnPosition( int in_row, int in_colomn, int in_newTriggerThreshold );

        int getTriggerThresholdOnPosition( int in_row, int in_colomn );

        int getTotalNumberOfEffects();

        int makeSnapshotOfEffects();

        void setEffectTriggerThresholdOnClientOnPositionToValue( int in_row, int in_colomn, int in_newTriggerThreshold );

        void manuallyTriggerEffectOnClientOnPosition( int in_row, int in_colomn );

        Position getPositionOfClientWithThisEffect( WaterDrop* in_effect );

        std::string getChipIDOfClientWithThisEffect( WaterDrop* in_effect );

        bool getHasClientSetOnPosition( int in_row, int in_colomn ){
            bool output = false;
            _mutex_bateriaVector.lock();
            if( _bateriaVector[in_row][in_colomn] != NULL ){
                output = true;
            }
            _mutex_bateriaVector.unlock();
            return output;
        }

    private:

        std::vector< std::vector< Client* > > _bateriaVector; // from now on, the mestre is just another client in row 0 (or where ever)
        std::mutex _mutex_bateriaVector;
        bool _learnMode = false;
        Client** _learnClient = NULL;
        std::vector< Client* > _markedForAction;
        int _rows = -1;
        int _colomns = -1;
        std::mutex _learnModeMutex;
        bool _someClientToInstrumentAssignmentHasChanged = false;
        std::map< Instrument, std::vector<Client*> > _sections;
};

class Log{

    // how to use this for reading:
    // call lockLog(),
    // call getSize() (optional),
    // call prepareForRead()
    // call getNextLine() n times (n = return value of getSize() ) and display the returned strings (or do what ever you like)
    // call unlockLog() (important!!)

    public:
        Log( int in_size ){
            for( int index = 0; index < in_size; index++ ){
                std::string temp = "";
                _log.push_back(temp);
            }
        };

        ~Log(){};

        void add( std::string in_line ){
            std::string timeStamp = getTimeOfDay();
            _lock.lock();
            _log[_writeIndex] = timeStamp + ": " + in_line;
            _lock.unlock();
            _writeIndex++;
            _writeIndex %= _log.size();
        }

        void lockLog(){
            _lock.lock();
        }

        int getSize(){
            return _log.size();
        }

        void prepareForRead(){
            _readIndex = _writeIndex;
        }

        std::string getNextLine(){
            std::string output = _log[_readIndex];
            _readIndex++;
            _readIndex %= _log.size();
            return output;
        }

        void unlockLog(){
            _lock.unlock();
        }

    private:
        std::vector< std::string > _log;
        int _writeIndex = 0;
        int _readIndex = 0;
        std::mutex _lock;

};

/*
class Effect{

    public:
        Effect(){};
        ~Effect(){};

        void run_loop(){
            _active = true
            while( _active ){

            }
        };


        void stop(){
            _active = false;
        };

        void trigger();

    private:
        bool _active = false;

};
 */

//class WaterDrop: public Effect{
class WaterDrop{

    public:
        WaterDrop( Position in_positionOfSignalSourceClient, int in_triggerThreshold, int in_speed, ColorSet in_color, bool in_stayOnColor, int in_attack, int in_release ){
            if( _bateria != NULL ){

                _triggerThreshold = in_triggerThreshold;
                _speed = in_speed;
                calculatePropagationDelay();

                _stayOnColor = in_stayOnColor;
                _attack = in_attack;
                _release = in_release;
                _color = in_color;

                buildSendString();

                int maxRowOfBateria = _bateria->getNumberOfRows();
                int maxColomnOfBateria = _bateria->getNumberOfColomns();

                int maxDistance = 0;
                int distanceFromCenterToOtherClient = 0;
                for (size_t rowIndex = 0; rowIndex < maxRowOfBateria; rowIndex++) {
                    for (size_t colomnIndex = 0; colomnIndex < maxColomnOfBateria; colomnIndex++) {
                        int deltaRow = rowIndex - in_positionOfSignalSourceClient.row;
                        int deltaColomn = colomnIndex - in_positionOfSignalSourceClient.colomn;
                        distanceFromCenterToOtherClient = (int) sqrt( (int) (pow(deltaRow, 2) + pow(deltaColomn, 2) + 0.5) );
                        if (distanceFromCenterToOtherClient > maxDistance) {
                            maxDistance = distanceFromCenterToOtherClient;
                        }
                    }
                }

                std::vector< std::pair<Client **, sem_t> > emptySphere;
                for( size_t index = 0; index <= maxDistance; index++ ){
                    _spheres.push_back( emptySphere );
                }

                for( size_t rowIndex = 0; rowIndex < maxRowOfBateria; rowIndex++ ){
                    for( size_t colomnIndex = 0; colomnIndex < maxColomnOfBateria; colomnIndex++ ){
                        int deltaRow = rowIndex - in_positionOfSignalSourceClient.row;
                        int deltaColomn = colomnIndex - in_positionOfSignalSourceClient.colomn;
                        distanceFromCenterToOtherClient = (int) sqrt( (int) (pow(deltaRow, 2) + pow(deltaColomn, 2) + 0.5) );
                        std::pair< Client**, sem_t > anotherTempPair;
                        Client* pointerToClientAsItIsInBateria = _bateria->getPointerOfClientOnPosition( rowIndex, colomnIndex );
                        anotherTempPair.first = _bateria->getPointerToClientPointerOnPosition( rowIndex, colomnIndex );
                        sem_t anotherTempSemaphore;
                        int success = sem_init( &anotherTempSemaphore, 0, 1 );        // int sem_init(sem_t *sem, int pshared, unsigned int value); ... If pshared has the value 0, then the semaphore is shared between the threads of a process, and should be located at some address that is visible to all threads (e.g., a global variable, or a variable allocated dynamically on the heap).The value argument specifies the initial value for the semaphore.
                        if( success == -1 ){
                            print( "WaterDrop(): error on creating Semaphore!" );
                        }
                        sem_wait( &anotherTempSemaphore );              // we call this here, to have the each semaphore locked. by that it needs a trigger() to get something done...
                        anotherTempPair.second = anotherTempSemaphore;
                        _spheres[distanceFromCenterToOtherClient].push_back( anotherTempPair );
                    }
                }
            }else{
                print("_bateria == NULL");
            }
            print("leaving WaterDrop::WaterDrop()");
        }

        ~WaterDrop(){

            print("entering WaterDrop::~WaterDrop()");
            for( size_t sphereIndex = 0; sphereIndex < _spheres.size(); sphereIndex++ ){
                for( size_t clientIndex = 0; clientIndex < _spheres[sphereIndex].size(); clientIndex++ ){
                    sem_destroy( &(_spheres[sphereIndex][clientIndex].second) );
                }
            }
            print("leaving WaterDrop::~WaterDrop()");
        }

        void calculatePropagationDelay(){
            _propagationDelayMilliSeconds = (int) (((float) 1 / (float) _speed) * 1000); //the result is something in milliseconds
        }

        void singleThread_working( int in_sphereIndex, int in_clientIndex );

        void start(){   // creating all threads (one for each client in bateria), and they will wait for trigger() to be called
            _active = true;
            for( size_t sphereIndex = 0; sphereIndex < _spheres.size(); sphereIndex++ ){
                for( size_t clientIndex = 0; clientIndex < _spheres[sphereIndex].size(); clientIndex++ ){
                    std::thread tempThread( &WaterDrop::singleThread_working, this, sphereIndex, clientIndex );
                    tempThread.detach();
                }
            }
        }

        void stop(){
            _active = false;
            // but each singleThread_working() is still waiting for the semaphores... so we call them one last time, it wont do anything, because _active is already == false
            for( size_t sphereIndex = 0; sphereIndex < _spheres.size(); sphereIndex++ ){
                for( size_t clientIndex = 0; clientIndex < _spheres[sphereIndex].size(); clientIndex++ ){
                     sem_post( &(_spheres[sphereIndex][clientIndex].second) );
                }
            }
        }

        void trigger(){
            if( _active ){
                for (size_t sphereIndex = 0; sphereIndex < _spheres.size(); sphereIndex++) {
                    //print( "spereIndex: " + to_string(sphereIndex) );
                    for (size_t clientIndex = 0; clientIndex < _spheres[sphereIndex].size(); clientIndex++){
                        sem_t* semaphore = &(_spheres[sphereIndex][clientIndex].second);
                        print( "spheres[" + to_string( sphereIndex ) + "][" + to_string(clientIndex) + "]: semaphore: " + to_string(semaphore) );
                        sem_post( semaphore ); // singleThread_working() is waiting for that...
                    }
                    usleep(_propagationDelayMilliSeconds * 1000);
                }
            }
            print("leaving WaterDrop::trigger()");
        }

        bool isActive(){
            return _active;
        }

        ColorSet getColorOfEffect(){
            return _color;
        }

        bool getHasRandomColor(){
            bool output = false;
            if( _color.randomRed && _color.randomGreen && _color.randomBlue && _color.randomUV ){
                output = true;
            }
            return output;
        }

        int getEffectSpeed(){
            return _speed;
        }

        bool getEffectStayOnColor(){
            return _stayOnColor;
        }

        int getEffectAttack(){
            return _attack;
        }

        int getEffectRelease(){
            return _release;
        }

        bool edit( ColorSet in_color, int in_speed, int in_stayOnColor, int in_attack, int in_release ){
            bool output = false;
            _speed = in_speed;
            calculatePropagationDelay();
            _stayOnColor = in_stayOnColor;
            _color = in_color;
            _attack = in_attack;
            _release = in_release;
            output = buildSendString();
            return output;
        }

        bool buildSendString(){
            bool output = false;
            if( _stayOnColor ){
                _toSendMessage = _messageGenerator->getFadeToColorInSteps( _color, _attack );
                output = true;
            }else{
                _toSendMessage = _messageGenerator->getFadeWithAttackAndReleaseAndBackToOff( _color, _attack, _release );
                output = true;
            }
            return output;
        }

        std::string getTitle(){
            return "WaterDrop FX";
        }

        int getTriggerThreshold(){
            return _triggerThreshold;
        }

        void setTriggerThreshold( int in_newTriggerThreshold ){
            _triggerThreshold = in_newTriggerThreshold;
        }

        std::string getEffectName(){
            return "waterDropFX";   // we get other effects later. this will be changed and make sense then
        }

    private:
        ColorSet _color;
        std::string _toSendMessage = "";
        //bool _randomColor = false;
        bool _stayOnColor = false;
        int _triggerThreshold = 0;
        int _speed = -1;
        int _attack = -1;
        int _release = -1;
        std::vector< std::vector < std::pair< Client**, sem_t > > > _spheres;
        int _propagationDelayMilliSeconds = -1;
        bool _active = false;

};

class Client{

    public:
        Client( std::string in_identifyString, std::string in_ipAdress, std::vector<Command> in_remainingCommandsFromIdentifyingProcess, std::string in_remainingStringFromIdentifyingProcess ){
            //print( "entering Client::Client()" );

            _ipAdress = in_ipAdress;
            _remainingCommandsFromIdentificationProcess = in_remainingCommandsFromIdentifyingProcess;
            _remainingString = in_remainingStringFromIdentifyingProcess;

            // in_identifyString will look like this: 536527_Nov 17 2017_02:29:07

            bool chipID_found = false;
            std::string chipID = "";
            std::string date = "";
            char symbol;
            for( size_t index = 0; index < in_identifyString.length(); index++ ){
                symbol = in_identifyString.at(index);
                if( symbol == '_' && chipID_found == false ){
                    chipID_found = true;
                    index++;
                    symbol = in_identifyString.at(index);
                }
                if( chipID_found == false ){
                    chipID += symbol;
                }else{
                    date += symbol;
                }
            }

            _chipID = chipID;
            _version = date;

            //sem_init( &_messageSemaphore, 0, 0 );     //like this???
            int success = sem_init( &_messageSemaphore, 0, 1);        // or this??
            if( success == -1 ){
                print( "Client(): sem_init() failed!" );
            }

            _log = new Log(32);

            bzero( _emptyMessageForCompare, READ_MESSAGE_BUFFER );
            //print( "leaving Client::Client()" );
        }

        ~Client(){
            delete _log;
            sem_destroy(&_messageSemaphore);
        }

        unsigned long int getLastSeenUnixTime(){
            return _lastSeenUnixTime;
        }

        void setInstrument( Instrument in_instrument ){
            _instrument = in_instrument;
        }

        Instrument getInstrument(){
            return _instrument;
        }

        void setNewIp( std::string in_ipAdress ){
            _ipAdress = in_ipAdress;
        }

        void updateLastSeen(){
            _lastSeen = getTimestampWithDateAndTime();
            _lastSeenUnixTime = getUnixTime();
        }

        void setSocketFileDescriptor( int64_t in_socketFileDescriptor ){

            while( _socketFileDescriptor != -1 ){   //there might be the case, that a client has disconnected, but before this client is marked as offline proberly (by setting the _socketFiledescriptor to -1) the client re-connects and this methode is called again to set a new fd. that's why we wait here for the fd to be -1... (which will be set in run_loop() after all loop-threads terminated
                usleep(100);
                //print("waiting for _incommingClients_socketFileDescriptor to be set to -1 ...");
            }

            //print("setting _incommingClients_socketFileDescriptor of Client[" + _chipID + "] to " + to_string(in_socketFileDescriptor));
            _socketFileDescriptor = in_socketFileDescriptor;

            struct timeval timeout;
            timeout.tv_sec = SOCKET_READ_TIMEOUT_SEC;
            timeout.tv_usec = 0;
            setsockopt(_socketFileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

            _isConnected = true;
        }

        bool getIsConnected(){
            return _isConnected;
        }

        std::string getChipID(){
            return _chipID;
        }

        void markClientAsDisconnected(){
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!! entering markClientAsDisconnected()");
            _isConnected = false;
            sem_post( &_messageSemaphore ); // up   -> we need to do this here, to get the client out of run_loop()-loop
        }

        bool setToColor( ColorSet in_colorSet ){
            bool output = false;
            std::string toSendString = _messageGenerator->getFadeToColorInSteps( in_colorSet, 1 );
            output = addMessageToStack( toSendString );
            return output;
        }

        std::string getVersion(){
            return _version;
        }

        std::string getLastSeen(){
            return _lastSeen;
        }

        std::string getSignalStrength(){
            return _signalStrength;
        }

        std::string getIpAdressAsString(){
            std::string output = _ipAdress;
            return output;
        }

        std::string getLastState(){
            std::string output = _lastState;
            return output;
        }

        std::string getChainLength(){
            std::string output = to_string( _chainLength );
            return output;
        }

        std::string getEffectName(){
            std::string output = "not set";
            if( _effect != NULL ){
                output = _effect->getTitle();
            }
            return output;
        }

        Log* getLog(){
            return _log;
        }

        void addLineToLog( std::string in_line ){
            _log->add(in_line);
        }

        /*
        void stopEffect(){
            _effect->stop();
        }
         */

        bool setNewEffect( WaterDrop* in_effect ){
            bool output = false;
            if( _effect != NULL ){
                delete _effect;
            }
            _effect = in_effect;
            if( _messageGenerator != NULL ){
                output = true;
                addMessageToStack( _messageGenerator->getSetTriggerThreshold( _effect->getTriggerThreshold() ) );
            }else{
                print("setNewEffect(): _messageGenerator == NULL");
            }
            return output;
        }

        void run_loop(){
            //print( "entering Client::run_loop()" );
            runLoopMutex.lock();
            std::thread waitForMessageThread( &Client::waitForMessagesLoop, this );
            //waitForMessageThread.detach();    we join the threads later..
            std::thread sendPingThread( &Client::sendPingLoop, this );
            //sendPingThread.detach();          we join the threads later..
            while( _isConnected ){              // the two thread above will not do anything here, since they only coming back to here, if _isConnected == false ...

                sem_wait( &_messageSemaphore ); // down
                //usleep(40000);

                mutex_outgoingMessagesStack.lock();
                //print("_outgoingMessagesStack: " + to_string(&_outgoingMessagesStack));
                for( int index = 0; index < _outgoingMessagesStack.size(); index++ ){
                    std::string message = _outgoingMessagesStack[index];
                    //print("!----------! message: " + message);
                    sendMessage( message );
                }
                _outgoingMessagesStack.clear();
                mutex_outgoingMessagesStack.unlock();
            }
            waitForMessageThread.join();
            sendPingThread.join();
            runLoopMutex.unlock();
            _socketFileDescriptor = -1; //we will only reach this point, after setting the client to _isConnected = false
            print( "leaving Client::run_loop()" );
        }

        bool addMessageToStack( std::string in_message ){
            _outgoingMessagesStack.push_back( in_message );
            sem_post( &_messageSemaphore ); // up
            return true;
        }

        bool getHasActiveEffect(){
            bool output = false;
            if( _effect != NULL ){
                output = _effect->isActive();
            }
            return output;
        }

        bool getHasEffect(){
            bool output = false;
            if( _effect != NULL ){
                output = true;
            }
            return output;
        }

        bool setEffectState( bool in_state ){
            bool output = false;
            if( _effect != NULL ){
                if( _effect->isActive() ){
                    _effect->stop();
                }else{
                    _effect->start();
                }
                output = true;
            }
            return output;
        }

        ColorSet getColorOfEffect(){
            ColorSet output;
            if( _effect != NULL ){
                output = _effect->getColorOfEffect();
            }else{
                print("Client::getColorOfEffect(): requested Client has no effect set");
            }
            return output;
        }

        bool getEffectHasRandomColor(){
            bool output = false;
            if( _effect != NULL ){
                output = _effect->getHasRandomColor();
            }else{
                print("Client::getEffectHasRandomColor(): requested Client has no effect set");
            }
            return output;
        }

        int getEffectSpeed(){
            int output = -1;
            if( _effect != NULL ){
                output = _effect->getEffectSpeed();
            }else{
                print("Client::getEffectSpeedOnPosition(): requested Client has no effect set");
            }
            return output;
        }

        int getEffectStayOnColor( ){
            int output = -1;
            if( _effect != NULL ){
                output = _effect->getEffectStayOnColor();
            }else{
                print("Client::getEffectSpeedOnPosition(): requested Client has no effect set");
            }
            return output;
        }

        int getEffectAttackOnPosition(){
            int output = -1;
            if( _effect != NULL ){
                output = _effect->getEffectAttack();
            }else{
                print("Client::getEffectAttackOnPosition(): requested Client has no effect set");
            }
            return output;
        }

        int getEffectReleaseOnPosition(){
            int output = -1;
            if( _effect != NULL ){
                output = _effect->getEffectRelease();
            }else{
                print("Client::getEffectReleaseOnPosition(): requested Client has no effect set");
            }
            return output;
        }

        bool editWaterDropEffect( ColorSet in_color, int in_speed, int in_stayOnColor, int in_attack, int in_release ){
            bool output = false;
            if( _effect != NULL ){
                output = _effect->edit( in_color, in_speed, in_stayOnColor, in_attack, in_release );
            }else{
                print( "Client::editWaterDropEffect(): requested Client has no effect" );
            }
            return output;
        }

        void setChainLength( int in_newChainLength ){
            _chainLength = in_newChainLength;
            //addMessageToStack( "@setChainLength;" + to_string( in_newChainLength ) + "#" );
            sendSavedChainLengthInformation();
        }

        bool sendTriggerThresholdInformation(){
            int output = false;
            if( _effect != NULL ){

                if( _messageGenerator != NULL ){
                    std::string message = _messageGenerator->getSetTriggerThreshold( _effect->getTriggerThreshold() );
                    output = addMessageToStack( message );
                }else{
                    print("sendTriggerThresholdInformation(): _messageGenerator == NULL");
                }
            }else{
                print("_effect == NULL");
            }
            return output;
        }

        bool setTriggerThresholdOfEffect( int in_newTriggerThreshold ){
            bool output = false;
            if( _effect != NULL ){
                _effect->setTriggerThreshold( in_newTriggerThreshold );
                output = sendTriggerThresholdInformation();
            }else{
                print("_effect == NULL");
            }
            return output;
        }

        int getTriggerThresholdOfEffect(){
            int output = -1;
            if( _effect != NULL ){
                output = _effect->getTriggerThreshold();
            }else{
                print("_effect == NULL");
            }
            return output;
        }

        void sendSavedChainLengthInformation(){
            addMessageToStack( "@setChainLength;" + to_string( _chainLength ) + "#" );
        }

        WaterDrop* getEffectPointer(){
            WaterDrop* output = _effect;
            return output;
        }

    private:

        void waitForMessagesLoop(){

            //there might be remaining commands from identification process
            for( int index = 0; index < _remainingCommandsFromIdentificationProcess.size(); index++ ){
                processCommand( _remainingCommandsFromIdentificationProcess[index] );
            }

            while(_isConnected) {
                std::string message = getMessageFromSocket();  // has timeout
                if( message != "" ){
                    std::vector<Command> commands = extractCommand(message, &_remainingString);
                    for (int index = 0; index < commands.size(); index++) {
                        processCommand( commands[index] );
                    }
                }else{
                    print("empty message received!");   // this can happen after a read-timeout, which is not unlikely...
                    usleep(100000);
                }
            }
            print("closing _incommingClients_socketFileDescriptor");
            close(_socketFileDescriptor);

        }

        void processCommand( Command in_command ){

            _log->add( "received: " + in_command.type + " ... " + in_command.content );
            //print( "received: " + in_command.type + " ... " + in_command.content );

            if( in_command.type == "identify" ) {
                if( _chipID == "notSet" ){
                    _chipID = in_command.content;
                }else{
                    print( "received identify-message from Client " + _chipID + ", but ID was already set (as you can see on this message)" );
                }
            }else if( in_command.type == "ping" ){
                handlePing( &in_command.content );
            }else if( in_command.type == "button0" ) {
                processButtonPressed(&in_command.content);
            }else if( in_command.type == "trigger" ){
                processTrigger();
            }else if( in_command.type == "state" ){
                _lastState = in_command.content;
            /*
            }else if( in_command.type == "chainLength" ){
                _chainLength = in_command.content;
            */
            }else{
                print( "unknown command ... commandType: " + in_command.type + " ... commandContent: " + in_command.content );
            }
        }

        void handlePing( const std::string* in_strengthInformation ){
            _signalStrength = *in_strengthInformation;
        }

        void processTrigger() {
            print("entering Client::processTrigger()");
            if( _bateria != NULL ){
                if( _effect != NULL ){
                    _effect->trigger();
                }else{
                    print("_effect == NULL");
                }
            }else{
                print("incomming trigger... but bateria is not initiated yet!!!!! ");
            }
        }

        void processButtonPressed( const std::string* in_commandContent ){
            //print("entering processButtonPressed()");
            if( *in_commandContent == "positiveEdge" ){
                print("Client::processButtonPressed(): button0 positive edge.");
                if( _bateria != NULL ) {
                    if( _effect != NULL ){
                        _effect->trigger();
                    }else if( _bateria->getLearnMode() ){
                        _bateria->learnClient(this);
                    }else{
                        //print("button0 positive edge... but bateria is not in learnMode... ");
                    }
                }else{
                    //print("button0 positive edge... but bateria is not initiated yet!!!!! ");
                }
            }else if( *in_commandContent == "negativeEdge" ){
                print( "Client::processButtonPressed(): button0 negative edge. nothing set yet... " );
            }else{
                print( "error ... processButtonPressed(): in_commandContent: " + *in_commandContent );
            }
            //print("leaving processButtonPressed()");
        }

        std::string getMessageFromSocket(){           //get called by startThreadForWaitingForMessages()

            //print(" entering client::waitForMessageFromClient()");
            std::string Output = "";
            int64_t successfullyTransmitted = 0;               //return value for the read() and write() calls; i.e. it contains the number of characters read or written.
            char receivedMessage_char[READ_MESSAGE_BUFFER];    //The server reads characters from the socket connection into this buffer.

            bzero( receivedMessage_char, READ_MESSAGE_BUFFER );       //initializes the buffer using the bzero() function,

            /*
            char _emptyMessageForCompare[READ_MESSAGE_BUFFER];                   //this is needed some lines below to check, if the reading was successful and
            strcpy(_emptyMessageForCompare, receivedMessage_char);
             */

            if( _isConnected ){

                try{
                    //print("client::waitForMessageFromClient(): trying to read from client " + _chipID + " ; ClientIsConnected: " + to_string(_isConnected) + " (1=true; 0=false)" + " _incommingClients_socketFileDescriptor: " + to_string(_incommingClients_socketFileDescriptor));
                    successfullyTransmitted = read( _socketFileDescriptor, receivedMessage_char, READ_MESSAGE_BUFFER - 1 ); // has timeout!
                    //print( "receivedMessage_char: " + to_string(receivedMessage_char) + " ... " + "successfullyTransmitted: " + to_string(successfullyTransmitted) );
                    //print("read finished");
                }catch(...){
                    print( "waitForMessageFromClient(): catching at reading from Socket" );
                    markClientAsDisconnected();
                }                                                                                                                     //there is something for it to read in the socket, i.e. after
                //the client has executed a write(). It will read either the
                //total number of characters in the socket or 255, whichever
                //is less, and return the number of characters read. The read()
                //man page has more information.

                //print( "client::waitForMessageFromClient(): Client: " + _chipID +  " ; Successfullytransmitted: " + to_string(successfullyTransmitted) + " ; ReceivedMessage: " + receivedMessage_char );
            }

            if ( successfullyTransmitted < 0 && _isConnected ){                                        // so i copy it to this place but without the exit(1)
                //this can happen after a read-timeout. this means nothing, because the timeout is very short...
            }

            if( strcmp( _emptyMessageForCompare, receivedMessage_char ) == 0 && _isConnected ){                          //i did this an the if(ClientIsConnected) above to finnally get rid of the problem, that if the first connected client disconnected made the server crash
                //this can happen after a read-timeout...

                print( "client::waitForMessageFromClient(): reading an empty Message from Client " +  _chipID );

                /*
                markClientAsDisconnected();
                //*/
            }


            /*!
            if(successfullyTransmitted == 0 && _isConnected ){
                markClientAsDisconnected(); // it seems, this can happen after a client is turned off, but not already marked as disconnected
                //print("Client " + _chipID + ": waitForMsg(): Client " + _chipID + " disconnected. set ClientIsConnected = 'false'");
            }
            */


             /*
            if ( successfullyTransmitted < 0 && _isConnected ){                                        // so i copy it to this place but without the exit(1)
                //print("client::waitForMsg(): ERROR reading from socket");
                markClientAsDisconnected();
            }
             */

            if( _isConnected && (successfullyTransmitted != -1) ){ // in case, none of the upper conditions marked the client as disconnected...
                updateLastSeen();
            }

            Output = to_string(receivedMessage_char);

            //print("leaving client::waitForMessageFromClient()");

            return Output;

        }

        void sendMessage( std::string in_string ){

            _log->add("sending: " + in_string);

            /*
            char toSendMessage_char[65536];         // 2^16, should be enough
            bzero(toSendMessage_char, 65536);
            strcpy( toSendMessage_char, in_string.c_str() );
            */

            int bytesWritten = -1;
            try{          //well lets try to catch the case, that the client has disconnected
                if(_isConnected){                  //i guess i need to check that here again, because the status of the client can has changed in the meantime.


                    // down belos is a write()-call. i onced received a signal 13 (SIGPIPE) here. to avaoid that, we check now for a closed socket.
                    // i try the way from here: http://libevent-users.monkey.narkive.com/1EBKF7UJ/process-terminating-with-default-action-of-signal-13-sigpipe
                    // well. it makes sense, that checking for a closed socket is not a good solution, because after checking (and still open) the remote-side could already have closed it.
                    // so i copy this way: "The only sane way is ignoring SIGPIPE. SIGPIPE is fine for Unix pipe filter programs."
                    // well.. actually i can not find a proper way to just ignore the SIGPIPE... instead i just write an empty handler... yea... that will do... swayyyze!!!


                    //print( "trying to write client[" + to_string(_chipID) + "] ... message: " + in_string);
                    bytesWritten = write( _socketFileDescriptor, in_string.c_str(), strlen( in_string.c_str() ) );




                }
            }catch(...){
                markClientAsDisconnected();
                //print(" Client " + _chipID + ": 'catch'ing writing error!");
            }

            if (bytesWritten < 0 && _isConnected  ){   //error-handling
                // well... this seems to be possible after a client get's turned off and on very quickly... hm... all this re-connecting and stuff is very complicated. but right now, it seems to work, so i keep this like it is...
                print( "\nERROR writing to socket, somethin went veeeeerrrrrryyy wrong! ; variable 'bytesWritten' = " + to_string(bytesWritten) );
                markClientAsDisconnected();
            }

            _timeLastMessageWasSent = getCurrentTime();

        }

        void sendPingLoop(){
            while( _isConnected ){
                usleep( MIN_TIME_BETWEEN_PINGS * 1000 );
                addMessageToStack( "@ping;-#" );
                _timeLastMessageWasSent = getCurrentTime();
            }
        }

        char _emptyMessageForCompare[READ_MESSAGE_BUFFER];
        std::vector<Command> _remainingCommandsFromIdentificationProcess;
        std::string _remainingString = "";
        WaterDrop* _effect = NULL;
        Instrument _instrument = unknown;
        std::string _lastState = "notSet";
        std::string _ipAdress = "notSet";
        std::string _lastSeen = "notSet";
        unsigned long int _lastSeenUnixTime = 0;
        bool _isConnected = false;
        int _socketFileDescriptor = -1;
        unsigned long _timeLastMessageWasSent = 0;
        std::string _chipID = "notSet";
        std::string _version = "notSet";
        int _chainLength = 1;   // the default is just one NEO-Pixel. this variable can be set via user-interface and will be send to the client right after set and each time the client reconnects...
        std::string _signalStrength = "notSet";
        sem_t _messageSemaphore;
        std::vector<std::string> _outgoingMessagesStack;
        std::mutex mutex_outgoingMessagesStack;
        Log* _log = NULL;
        std::mutex runLoopMutex;

};

bool Bateria::setMarkedClientsToColor( ColorSet in_colorSet ){              // must be here, otherwise compiler complains...
    bool output = true;
    for( size_t index = 0; index < _markedForAction.size() && output == true; index++ ){
        Client* temp_client = _markedForAction[index];
        output = temp_client->setToColor( in_colorSet );
    }
    return output;
}

void Bateria::sendFadeSeriesToClientOnPosition( Position in_position, std::string in_fadeSeries ){
    _mutex_bateriaVector.lock();
    Client* temp = _bateriaVector[in_position.row][in_position.colomn];
    _mutex_bateriaVector.unlock();
    if( temp != NULL ){
        temp->addMessageToStack(in_fadeSeries);
    }else{
        print("can not send fade to client on position, since there is no client set!");
    }
}

Instrument Bateria::getInstrumentOfClientWithChipID( std::string in_chipID ){
    Instrument output = unknown;
    bool found = false;
    _mutex_bateriaVector.lock();
    for (size_t rowIndex = 0; rowIndex < _bateriaVector.size() && found == false; rowIndex++) {
        for (size_t colomnIndex = 0; colomnIndex < _bateriaVector[rowIndex].size() && found == false; colomnIndex++) {
            Client* temp_clientOnPosition = _bateriaVector[rowIndex][colomnIndex];
            if( temp_clientOnPosition != NULL ){
                std::string chipIDonPosition = temp_clientOnPosition->getChipID();
                if (chipIDonPosition == in_chipID) {
                    output = _bateriaVector[rowIndex][colomnIndex]->getInstrument();
                    found = true;
                }
            }
        }
    }
    _mutex_bateriaVector.unlock();
    return output;
}

bool Bateria::setInstrumentOfChipID( std::string in_chipID, Instrument in_instrument ){
    bool output = false;
    _mutex_bateriaVector.lock();
    for( size_t rowIndex = 0; rowIndex < _bateriaVector.size(); rowIndex++ ){
        for( size_t colomnIndex = 0; colomnIndex < _bateriaVector[rowIndex].size(); colomnIndex++ ) {
            Client *tempClient = _bateriaVector[rowIndex][colomnIndex];
            if( tempClient != NULL ){
                if (stringCompare(tempClient->getChipID(), in_chipID)) {
                    _bateriaVector[rowIndex][colomnIndex]->setInstrument( in_instrument );
                    output = true;
                }
            }
        }
    }
    _mutex_bateriaVector.unlock();
    return output;
}

void Bateria::restartAllActiveEffects(){
    for( int rowIndex = 0; rowIndex < _bateriaVector.size(); rowIndex++ ){
        for( int colomnIndex = 0; colomnIndex < _bateriaVector[rowIndex].size(); colomnIndex++ ){
            if( _bateriaVector[rowIndex][colomnIndex] != NULL ){
                if( _bateriaVector[rowIndex][colomnIndex]->getHasEffect() ){
                    WaterDrop* effect = _bateriaVector[rowIndex][colomnIndex]->getEffectPointer();
                    if( effect->isActive() ){
                        effect->stop();
                        effect->start();
                    }else{
                        //print("effect is already inactive");
                    }
                }else{
                    //print("has no effect");
                }
            }else{
                //print("_bateriaVector[rowIndex][colomnIndex] == NULL");
            }
        }
    }
}

std::string Bateria::getInstrumentOnPosition( Position in_position ){
    std::string output = "";
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_position.row][in_position.colomn] != NULL ){
        Instrument currentInstrument= _bateriaVector[in_position.row][in_position.colomn]->getInstrument();
        if( currentInstrument == unknown ){
            output = "unknown";
        }else if( currentInstrument == mestre ){
            output = "mestre";
        }else if( currentInstrument == caixa ){
            output = "caixa";
        }else if( currentInstrument == tamborim ){
            output = "tamborim";
        }else if( currentInstrument == repique ){
            output = "repique";
        }else if( currentInstrument == chocalho ){
            output = "chocalho";
        }else if( currentInstrument == primera ){
            output = "primera";
        }else if( currentInstrument == segunda ){
            output = "segunda";
        }else if( currentInstrument == tetiera ){
            output = "tetiera";
        }else if( currentInstrument == agogo ){
            output = "agogo";
        }else{
            print("Bateria::getInstrumentOnPosition(): Instrument not available");
        }
    }else{
        output = "null";
    }
    _mutex_bateriaVector.unlock();
    return output;
}

bool Bateria::setInstrumentOnPosition( Position in_position, Instrument in_instrument ){
    //print("entering Bateria::setInstrumentOnPosition() ... in_position.row: " + to_string(in_position.row) + " ... in_position.colomn: " + to_string(in_position.colomn));
    bool output = true;
    _mutex_bateriaVector.lock();
    Client* clientOnThisPosition = _bateriaVector[in_position.row][in_position.colomn];
    if( clientOnThisPosition != NULL ){
        // looking for the client in an old section and delete this entery
        if( clientOnThisPosition->getInstrument() != unknown ){  // the client on this position already belonged to a section...
            Instrument oldSectionInstrument = _bateriaVector[in_position.row][in_position.colomn]->getInstrument();
            bool found = false;
            for (size_t index = 0; index < _sections[oldSectionInstrument].size() && found == false; index++) {
                if (_sections[oldSectionInstrument][index] == clientOnThisPosition) {
                    found = true;
                    //print("Bateria::setInstrumentOnPosition(): erasing Instrument on index: " + to_string(index) + " ... from oldSectionInstrument: " + to_string(oldSectionInstrument));
                    _sections[oldSectionInstrument].erase( _sections[oldSectionInstrument].begin() + index );
                }
            }
            if (found == false) {
                print("Bateria::setInstrumentOnPosition(): did not find old Instrument of client... damn that...");
            }
        }
        _bateriaVector[in_position.row][in_position.colomn]->setInstrument( in_instrument );
        _sections[in_instrument].push_back( clientOnThisPosition );
        _someClientToInstrumentAssignmentHasChanged = true;
    }else{
        output = false;
    }
    _mutex_bateriaVector.unlock();
    //print("leaving Bateria::setInstrumentOnPosition()");
    return output;
}

bool Bateria::setWaterDropEffectOnPosition( Position in_position, ColorSet in_color, int in_triggerThreshold, int in_speed, bool in_stayOnColor, int in_attack, int in_release ) {
    //print("entering Bateria::setWaterDropEffectOnPosition()");
    bool output = false;
    if( _bateria != NULL ){


        if( _bateriaVector[in_position.row][in_position.colomn] != NULL ){

            //WaterDrop* waterDropEffect = new WaterDrop( in_position, delay, in_color, in_stayOnColor, in_attack, in_release );
            WaterDrop *waterDropEffect = new WaterDrop(in_position, in_triggerThreshold, in_speed,
                                                       in_color, in_stayOnColor, in_attack,
                                                       in_release);
            _mutex_bateriaVector.lock();
            Client *client = _bateriaVector[in_position.row][in_position.colomn];
            _mutex_bateriaVector.unlock();
            if (client != NULL) {
                waterDropEffect->start();
                output = client->setNewEffect(waterDropEffect);
                client->addLineToLog("set new effect (waterDrop)");
            }
        }else{
            print( "_bateria[" + to_string(in_position.row) + "][" + to_string(in_position.colomn) + "] == NULL" );
        }
    }else{
        print("_bateria == NULL");
    }
    //print("leaving Bateria::setWaterDropEffectOnPosition()");
    return output;
}

bool Bateria::editWaterDropEffectOnPosition( Position in_position, ColorSet in_color, int in_speed, bool in_stayOnColor, int in_attack, int in_release ){
    bool output = false;
    if( _bateria != NULL ){
        _mutex_bateriaVector.lock();
        Client* client = _bateriaVector[in_position.row][in_position.colomn];   // is that atomic?
        _mutex_bateriaVector.unlock();
        if( client != NULL ) {
            output = client->editWaterDropEffect( in_color, in_speed, in_stayOnColor, in_attack, in_release );
        }else{
            print( "Bateria::editWaterDropEffectOnPosition(): requested client doesn't exist" );
        }
    }else{
        print("_bateria == NULL");
    }
    //print("leaving Bateria::editWaterDropEffectOnPosition()");
    return output;
}

bool Bateria::setWaterDropEffectOnChip( std::string in_chipID, ColorSet in_color, int in_triggerThreshold, int in_speed, bool in_stayOnColor, int in_attack, int in_release ){
    bool output = false;
    bool found = false;
    Position positionOfClient(-1, -1);
    _mutex_bateriaVector.lock();
    for( size_t rowIndex = 0; rowIndex < _bateriaVector.size() && found == false; rowIndex++ ){
        for( size_t colomnIndex = 0; colomnIndex < _bateriaVector[rowIndex].size() && found == false; colomnIndex++ ){
            Client** tempClient = &( _bateriaVector[rowIndex][colomnIndex] );
            if( tempClient != NULL ){
                if( (*tempClient)->getChipID() == in_chipID ){
                    found = true;
                    positionOfClient.row = rowIndex;
                    positionOfClient.colomn = colomnIndex;
                }
            }
        }
    }
    _mutex_bateriaVector.unlock();
    if( found ){
        output = setWaterDropEffectOnPosition( positionOfClient, in_color, in_triggerThreshold, in_speed, in_stayOnColor, in_attack, in_release );
    }
    return output;
}

bool Bateria::getHasActiveEffect( int in_row, int in_colomn ){
    bool output = false;
    _mutex_bateriaVector.lock();
    Client* clientOnPosition = _bateriaVector[in_row][in_colomn];
    _mutex_bateriaVector.unlock();
    if( clientOnPosition != NULL ){
        output = clientOnPosition->getHasActiveEffect();
    }
    return output;
}

bool Bateria::getHasEffect( int in_row, int in_colomn ){
    bool output = false;
    _mutex_bateriaVector.lock();
    Client* clientOnPosition = _bateriaVector[in_row][in_colomn];
    _mutex_bateriaVector.unlock();
    if( clientOnPosition != NULL ){
        output = clientOnPosition->getHasEffect();
    }
    return output;
}

bool Bateria::setEffectOnPositionToState( int in_row, int in_colomn, bool in_state ){
    bool output = false;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        output = _bateriaVector[in_row][in_colomn]->setEffectState(in_state);
        if( output == false ){
            print("no effect set on this position");
        }
    }
    _mutex_bateriaVector.unlock();
    return output;
}

int Bateria::getRedValueOfEffectOnPosition( int in_row, int in_colomn ){
    int output = -1;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getColorOfEffect().red;
        }else{
            print( "Bateria::getRedValueOfEffectOnPosition(): requested Client has no effect" );
        }
    }else{
        print("Bateria::getRedValueOfEffectOnPosition(): _bateriaVector[in_row][in_colomn] == NULL");
    }
    _mutex_bateriaVector.unlock();
    return output;
}

int Bateria::getGreenValueOfEffectOnPosition( int in_row, int in_colomn ){
    int output = -1;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getColorOfEffect().green;
        }else{
            print( "Bateria::getGreenValueOfEffectOnPosition(): requested Client has no effect" );
        }

    }else{
        print("Bateria::getGreenValueOfEffectOnPosition(): _bateriaVector[in_row][in_colomn] == NULL");
    }
    _mutex_bateriaVector.unlock();
    return output;
}

int Bateria::getBlueValueOfEffectOnPosition( int in_row, int in_colomn ){
    int output = -1;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getColorOfEffect().blue;
        }else{
            print( "Bateria::getBlueValueOfEffectOnPosition(): requested Client has no effect" );
        }
    }else{
        print("Bateria::getBlueValueOfEffectOnPosition(): _bateriaVector[in_row][in_colomn] == NULL");
    }
    _mutex_bateriaVector.unlock();
    return output;
}

int Bateria::getUVValueOfEffectOnPosition( int in_row, int in_colomn ){
    int output = -1;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getColorOfEffect().uv;
        }else{
            print( "Bateria::getUVValueOfEffectOnPosition(): requested Client has no effect" );
        }

    }else{
        print("Bateria::getUVValueOfEffectOnPosition(): _bateriaVector[in_row][in_colomn] == NULL");
    }
    _mutex_bateriaVector.unlock();
    return output;
}

bool Bateria::getEffectHasRandomColorOnPosition( int in_row, int in_colomn ){
    bool output = false;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getEffectHasRandomColor();
        }else{
            print( "Bateria::getEffectHasRandomColorOnPosition(): requested Client has no effect" );
        }
    }else{
        print("Bateria::getEffectHasRandomColorOnPosition(): _bateriaVector[in_row][in_colomn] == NULL");
    }
    _mutex_bateriaVector.unlock();
    return output;
}

int Bateria::getEffectSpeedOnPosition( int in_row, int in_colomn ){
    int output = -1;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getEffectSpeed();
        }else{
            print( "Bateria::getEffectHasRandomColorOnPosition(): requested Client has no effect" );
        }
    }else{
        print("Bateria::getEffectHasRandomColorOnPosition(): _bateriaVector[in_row][in_colomn] == NULL");
    }
    _mutex_bateriaVector.unlock();
    return output;
}

bool Bateria::getEffectStayOnColorOnPosition( int in_row, int in_colomn ){
    bool output = false;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getEffectStayOnColor();
        }else{
            print( "Bateria::getEffectStayOnColorOnPosition(): requested Client has no effect" );
        }
    }else{
        print("Bateria::getEffectStayOnColorOnPosition(): _bateriaVector[in_row][in_colomn] == NULL");
    }
    _mutex_bateriaVector.unlock();
    return output;
}

int Bateria::getEffectAttackOnPosition( int in_row, int in_colomn ){
    int output = -1;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getEffectAttackOnPosition();
        }else{
            print( "Bateria::getEffectStayOnColorOnPosition(): requested Client has no effect" );
        }
    }else{
        print("Bateria::getEffectStayOnColorOnPosition(): _bateriaVector[in_row][in_colomn] == NULL");
    }
    _mutex_bateriaVector.unlock();
    return output;
}

int Bateria::getEffectReleaseOnPosition( int in_row, int in_colomn ){
    int output = -1;
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn] != NULL ){
        bool hasEffect = _bateriaVector[in_row][in_colomn]->getHasEffect();
        if( hasEffect ){
            output = _bateriaVector[in_row][in_colomn]->getEffectReleaseOnPosition();
        }else{
            print( "Bateria::getEffectStayOnColorOnPosition(): requested Client has no effect" );
        }
    }else{
        print( "Bateria::getEffectStayOnColorOnPosition(): _bateriaVector[in_row][in_colomn] == NULL" );
    }
    _mutex_bateriaVector.unlock();
    return output;
}

void Bateria::setLEDchainLength( int in_newChainLength, std::string in_chipID ){
    bool found = false;
    _mutex_bateriaVector.lock();
    for( size_t rowIndex = 0; rowIndex < _bateriaVector.size() && found == false; rowIndex++ ){
        for( size_t colomnIndex = 0; colomnIndex < _bateriaVector[rowIndex].size() && found == false; colomnIndex++ ) {
            Client* tempClient = _bateriaVector[rowIndex][colomnIndex];
            if( tempClient != NULL ){
                if( stringCompare( tempClient->getChipID(), in_chipID ) ){
                    found = true;
                    tempClient->setChainLength( in_newChainLength );
                }
            }
        }
    }
    _mutex_bateriaVector.unlock();
}

int Bateria::setTriggerThresholdOnEffectOnPosition( int in_row, int in_colomn, int in_newTriggerThreshold ){
    _mutex_bateriaVector.lock();
    Client* client = _bateriaVector[in_row][in_colomn];
    _mutex_bateriaVector.unlock();
    int output = -1;
    if( client != NULL ){
        output = client->setTriggerThresholdOfEffect( in_newTriggerThreshold );
    }else{
        print( "Bateria::setTriggerThresholdOnEffectOnPosition(): client == NULL" );
    }
    return output;
}

int Bateria::getTriggerThresholdOnPosition( int in_row, int in_colomn ){
    int output = 0;
    _mutex_bateriaVector.lock();
    Client* client = _bateriaVector[in_row][in_colomn];
    _mutex_bateriaVector.unlock();
    if( client != NULL ){
        output = client->getTriggerThresholdOfEffect();
    }else{
        print( "Bateria::setTriggerThresholdOnEffectOnPosition(): client == NULL" );
    }
    return output;
}

int Bateria::getTotalNumberOfEffects(){
    int output = 0;
    _mutex_bateriaVector.lock();
    for( int row_index = 0; row_index < _bateriaVector.size(); row_index++ ){
        for( int colomn_index = 0; colomn_index < _bateriaVector[row_index].size(); colomn_index++ ){
            if( _bateriaVector[row_index][colomn_index]->getHasEffect() ){
                output++;
            }
        }
    }
    _mutex_bateriaVector.unlock();
    return output;
}

int Bateria::makeSnapshotOfEffects(){
    _snapshotMutex.lock();
    int output = -1;
    _effectSnapshotForInformationCollection.clear();
    _mutex_bateriaVector.lock();
    for( int row_index = 0; row_index < _bateriaVector.size(); row_index++ ){
        for( int colomn_index = 0; colomn_index < _bateriaVector[row_index].size(); colomn_index++ ){
            if( _bateriaVector[row_index][colomn_index] != NULL ){
                if( _bateriaVector[row_index][colomn_index]->getHasEffect() ){

                    WaterDrop* temp = _bateriaVector[row_index][colomn_index]->getEffectPointer();
                    _effectSnapshotForInformationCollection.push_back( temp );
                }
            }else{
                print("no Client set");
            }
        }
    }
    _mutex_bateriaVector.unlock();
    output = _effectSnapshotForInformationCollection.size();
    _snapshotMutex.unlock();
    return output;
}

void Bateria::setEffectTriggerThresholdOnClientOnPositionToValue( int in_row, int in_colomn, int in_newTriggerThreshold ){
    _mutex_bateriaVector.lock();
    _bateriaVector[in_row][in_colomn]->setTriggerThresholdOfEffect( in_newTriggerThreshold );
    _mutex_bateriaVector.unlock();
}

void Bateria::manuallyTriggerEffectOnClientOnPosition( int in_row, int in_colomn ){
    _mutex_bateriaVector.lock();
    if( _bateriaVector[in_row][in_colomn]->getHasEffect() ) {
        _bateriaVector[in_row][in_colomn]->getEffectPointer()->trigger();
    }
    _mutex_bateriaVector.unlock();
}

Position Bateria::getPositionOfClientWithThisEffect( WaterDrop* in_effect ){
    Position output(-1, -1);
    _mutex_bateriaVector.lock();
    bool found = false;
    for( int row_index = 0; row_index < _bateriaVector.size() && found == false; row_index++ ){
        for( int colomn_index = 0; colomn_index < _bateriaVector[row_index].size() && found == false; colomn_index++ ){
            if( _bateriaVector[row_index][colomn_index] != NULL ){
                WaterDrop *pointerInClient = _bateriaVector[row_index][colomn_index]->getEffectPointer();
                if (pointerInClient == in_effect) {
                    output.row = row_index;
                    output.colomn = colomn_index;
                    found = true;
                }
            }else{
                print( "_bateriaVector[row_index][colomn_index] == NULL" );
            }
        }
    }
    _mutex_bateriaVector.unlock();
    return output;
}

std::string Bateria::getChipIDOfClientWithThisEffect( WaterDrop* in_effect ){
    std::string output = "";
    bool found = false;
    _mutex_bateriaVector.lock();
    for( int row_index; row_index < _bateriaVector.size() && found == false; row_index++ ){
        for( int colomn_index; colomn_index < _bateriaVector[colomn_index].size() && found == false; colomn_index++ ){
            if( _bateriaVector[row_index][colomn_index]->getEffectPointer() == in_effect ){
                output = _bateriaVector[row_index][colomn_index]->getChipID();
                found = true;
            }
        }
    }
    _mutex_bateriaVector.unlock();
    return output;
}

void WaterDrop::singleThread_working(  int in_sphereIndex, int in_clientIndex ){
    if( _messageGenerator != NULL ){
        Client** client_ptr_ptr = ( _spheres[in_sphereIndex][in_clientIndex].first );
        if( client_ptr_ptr != NULL ){
            Client* client_ptr = *client_ptr_ptr;
            if( client_ptr != NULL ){
                while( _active ) {
                    sem_t *semaphore = &(_spheres[in_sphereIndex][in_clientIndex].second);
                    sem_wait(semaphore);  // down
                    print("semaphore: " + to_string(semaphore) + ": working-thread running... ");

                    if( _active ){ //at this point, the effect might actually be not active any more and will leave the while-loop anyways. but if stop() is called, the semaphore will be released and this thread will terminate at the end of this function. and we do not want the effect to actually happen
                        std::string toSendMessage = "";
                        if (_messageGenerator != NULL) {
                            (client_ptr)->addMessageToStack(_toSendMessage);
                        } else {
                            print("_messageGenerator == NULL");
                        }
                    }else{
                        print("effect is not active any more");
                    }

                }
            }else{
                print("client_ptr == NULL");
            }
        }else{
            print("client_ptr_ptr == NULL");
        }
    }else {
        print("_messageGenerator == NULL");
    }
    //print("leaving singleThread_working");
}

class GameOfLife{

    public:
        GameOfLife( std::vector< std::vector< std::pair< Client*, Instrument > > >* in_bateria ){
            _screen.clear();
            _height = in_bateria->size();
            _width = (*in_bateria)[0].size();
            for( size_t rowIndex; rowIndex < in_bateria->size(); rowIndex++ ){
                std::vector< std::pair< Client*, bool > > tempRow;
                std::vector< bool > workingCopyRow;
                for( size_t colomnIndex; colomnIndex < ( (*in_bateria)[rowIndex] ).size(); colomnIndex++ ){
                    std::pair< Client*, bool > tempPair;
                    tempPair.first = (*in_bateria)[rowIndex][colomnIndex].first;
                    tempPair.second = getRandomBool();
                    tempRow.push_back( tempPair );

                    bool workingCopyBool = false;
                    workingCopyRow.push_back(workingCopyBool);
                }
                _screen.push_back( tempRow );
                _workingCopy.push_back( workingCopyRow );
            }
        };

        ~GameOfLife(){};

        void shutdown(){
            _running = false;
        }

        void run(){

            _running = true;

            while( _running ){
                copyIntoWorkingcopy();
                for( size_t rowIndex; rowIndex < _screen.size(); rowIndex++ ){
                    for( size_t colomnIndex; colomnIndex < _screen[rowIndex].size(); colomnIndex++ ){
                        bool cellAlive = _workingCopy[rowIndex][colomnIndex];
                        int numberOfNeighbourCellsAlive = 0;
                        for( int checkingRowIndex = rowIndex-1; checkingRowIndex < rowIndex+1; checkingRowIndex++ ){
                            for( int checkingColomnIndex = colomnIndex-1; checkingColomnIndex < colomnIndex+1; checkingColomnIndex++ ){
                                if( checkingRowIndex != rowIndex && checkingColomnIndex != colomnIndex ){   // we don't want to look at our own cell..
                                    int checkingRowIndex_mod = checkingRowIndex % _height;
                                    int checkingColomnIndex_mod = checkingColomnIndex % _width;
                                    if (_workingCopy[checkingRowIndex_mod][checkingColomnIndex_mod] == true) {
                                        numberOfNeighbourCellsAlive++;
                                    }
                                }
                            }
                        }

                        if( cellAlive == false ){
                            if( numberOfNeighbourCellsAlive == 3 ){
                                _screen[rowIndex][colomnIndex].second = true;
                            }
                        }else{
                            if( numberOfNeighbourCellsAlive < 2 ){
                                _screen[rowIndex][colomnIndex].second = false;
                            }else if( numberOfNeighbourCellsAlive == 2 || numberOfNeighbourCellsAlive == 3 ){
                                //nothing...they stay alive
                            }else if( numberOfNeighbourCellsAlive > 3 ){
                                _screen[rowIndex][colomnIndex].second = false;
                            }
                        }
                    }
                }
                updateClients();
            }
        }

    private:

        void updateClients(){
            for( size_t rowIndex = 0; rowIndex < _screen.size(); rowIndex++ ){
                for( size_t colomnIndex = 0; colomnIndex < _screen[rowIndex].size(); colomnIndex++ ){
                    Client* tempClient = _screen[rowIndex][colomnIndex].first;
                    bool alive = _screen[rowIndex][colomnIndex].second;
                    if( _messageGenerator != NULL){
                        if( alive ){
                            tempClient->addMessageToStack( _messageGenerator->getFadeToColorInSteps( _messageGenerator->WHITE, 1 ) );
                        }else{
                            tempClient->addMessageToStack( _messageGenerator->getFadeToColorInSteps( _messageGenerator->OFF, 1 ) );
                        }
                    }else{
                        print("_messageGenerator == NULL");
                    }
                }
            }
        }

        void copyIntoWorkingcopy(){
            for( size_t rowIndex; rowIndex < _screen.size(); rowIndex++ ){
                for( size_t colomnIndex; colomnIndex < _screen[rowIndex].size(); colomnIndex++ ){
                    _workingCopy[rowIndex][colomnIndex] = _screen[rowIndex][colomnIndex].second;
                }
            }
        }

        void randomize(){
            for( size_t rowIndex; rowIndex < _screen.size(); rowIndex++ ){
                for( size_t colomnIndex; colomnIndex < _screen[rowIndex].size(); colomnIndex++ ){
                    _screen[rowIndex][colomnIndex].second = getRandomBool();
                }
            }
        };

        bool getRandomBool(){
            bool output;
            int randomValue = rand() % 2;
            if( randomValue == 0 ){
                output = false;
            }else{
                output = true;
            }
            return output;
        }

        bool _running = false;
        int _height = -1;
        int _width = -1;
        std::vector< std::vector< std::pair< Client*, bool > > > _screen;
        std::vector< std::vector<  bool > > _workingCopy;

};

class ConnectionManager{

    public:
        ConnectionManager(){
            _allClients.clear();
        };

        ~ConnectionManager(){};

        void sendMessageToAllClients( std::string in_string ){
            for( auto const &reference : _allClients ){
                Client* currentClient = reference.second;
                if( currentClient->getIsConnected() ){
                    //print("calling sendMessage for client " + currentClient->getChipID());
                    currentClient->addMessageToStack(in_string);
                }else{
                    //print("not sending message to Client[" + currentClient->getChipID() + "] tryed to send message to client, but client is marked as not connected!");
                }
            }
        }

        void waitForClients_Loop(){
            initSocketCommunication();
            _serverRunning = true;
            while( _serverRunning ){

                usleep(10000);
                //print("calling accept()");
                int64_t newSocketFileDescriptor = accept( _incommingClients_socketFileDescriptor, (struct sockaddr *) &_client_addr, &_sizeOfAdressOfClient );
                print( "client accepted!" );
                if( _serverRunning ){   // it is very likely, that the server get stopped (by clicking on the button in the UI). but then the server is still waiting for new clients here... so a new client can easily re-connect, so we need to check again, and if the server is stopped, we need to close the socket again.
                    if( newSocketFileDescriptor < 0 ){
                        _serverRunning = false;
                        print("something went veeeerryyy wrong! newSocketFileDescriptor < 0 after accepting!");
                    }

                    char ipAdress[16];
                    sprintf(ipAdress, "%s\n", inet_ntoa(_client_addr.sin_addr));
                    std::string ipAdress_str = to_string(ipAdress);

                    // watch out: the message from the client could be uncomplete and look like this:
                    // "@identify;536527_Nov 17 2017_02:29:07#@ping-38#@ping-38#@ping-38#@ping-38#@ping-3"
                    // right now, we just want to know about the identify-string. wo we ignore everything after that...

                    usleep(200000);   // giving the client time to to write the identify message...

                    std::string stringFromClient = readDirectlyFromSocket(newSocketFileDescriptor);
                    //print("stringFromClient: " + stringFromClient);

                    //debug (test)
                    //stringFromClient = "@identify;536527_Nov 17 2017_02:29:07#@ping-38#@ping-38#@ping-38#@ping-38#@ping-3";
                    //

                    std::string remainString = "";

                    std::vector<Command> receivedCommands = extractCommand( stringFromClient, &remainString );
                    std::string potentialChipID = "";
                    if (receivedCommands[0].type == "identify") {
                        potentialChipID = extractChipID(receivedCommands[0].content);
                        bool isAlreadyInMap = false;
                        if (_allClients.count(potentialChipID)) {
                            isAlreadyInMap = true;
                        }

                        if( isAlreadyInMap ){
                            //print("client with key " + receivedCommands[0].content + " already exists ... seting Client with new fd and run_loop...");
                            _allClients[potentialChipID]->setSocketFileDescriptor( newSocketFileDescriptor );
                            _allClients[potentialChipID]->setNewIp( ipAdress_str );   // very unlikely but: the ip-adress might have changed since this is the case were a client has disconnected and reconnected.
                            _allClients[potentialChipID]->updateLastSeen();
                            _allClients[potentialChipID]->sendSavedChainLengthInformation();
                        }else{
                            //print( "adding new Client to _allClients ... " );
                            std::string chipID_and_Version = receivedCommands[0].content;
                            receivedCommands.erase(receivedCommands.begin() + 0);
                            Client *newClient = new Client( chipID_and_Version, ipAdress_str, receivedCommands, remainString );
                            newClient->updateLastSeen();
                            _allClients.insert( make_pair(potentialChipID, newClient) );
                            newClient->setSocketFileDescriptor( newSocketFileDescriptor );
                        }
                        std::thread startClient( &Client::run_loop, _allClients[potentialChipID] );
                        startClient.detach();
                    } else {
                        print("new client faild with identification. closing socket...");
                        close( newSocketFileDescriptor );
                    }
                }else{
                    print("client tryed to connect, but server is not running");
                    close( newSocketFileDescriptor );
                }

            }

            print( "about to close accept-socketFileDescriptor" );        // do we really need that?
            close( _incommingClients_socketFileDescriptor );                              // ???

            print( "leaving waitForClients_Loop()" );
        }

        bool getServerRunning(){
            return _serverRunning;
        }

        int getNumberOfOnlineClients(){
            int output = 0;
            for(auto const &currentClient : _allClients){
                if( currentClient.second->getIsConnected() ){
                    output++;
                }
            }
            //print("ConnectionManager::getNumberOfOnlineClients(): output: " + to_string(output));
            return output;
        }

        void checkForOfflineClientsLoop(){
            //print( "entering checkForOfflineClientsLoop()" );
            while( _serverRunning ){
                //print("checking again for offline clients");
                for( auto const &currentClient : _allClients ){
                    if( currentClient.second->getIsConnected() ){
                        unsigned long int lastSeenOfClient = currentClient.second->getLastSeenUnixTime();
                        ///print("lastSeenOfClient: " + to_string(lastSeenOfClient));
                        unsigned long int currentTime = getUnixTime();
                        //print("currentTime: " + to_string(currentTime));
                        unsigned long int timeDelta = currentTime - lastSeenOfClient;
                        //print("timeDelta: " + to_string(timeDelta) + " ms");
                        if( timeDelta <= CLIENT_TIMEOUT ){
                            //! numberOfActiveClients++;
                        }else{
                            print("timeDelta > CLIENT_TIMEOUT ... calling markClientAsDisconnected()");
                            currentClient.second->markClientAsDisconnected();
                        }
                    }
                }
                //! _numberOfActiveClients = numberOfActiveClients;
                usleep( INTERVALL_CHECK_OFFLINE_CLIENTS * 1000 );
            }
            //print("leaving checkForOfflineClientsLoop()");
        }

        void shutdown(){
            for( auto const &reference : _allClients ){
                Client* currentClient = reference.second;
                if( currentClient->getIsConnected() ){
                    currentClient->markClientAsDisconnected();
                }else{
                    print("shutdown(): client not connected");
                }
            }
            //print("shutting down cpp-server");
            _serverRunning = false;
            usleep(500000);

            // well... the easiest way way to get the mainThread out of it's current accept()-state might be, to open another socket, which will be colsed from the mainThread again, because the server is already marked as not running... but the the loop will not continue because the condition is not true and it will leave the function...
            // so we pretend, we are a client... i know, it's ugly, but easy..
            struct sockaddr_in address;
            memset( &address, 0, sizeof(address) );
            address.sin_family = AF_INET;
            address.sin_port = htons( PORT );
            inet_pton(AF_INET, "127.0.0.1", &(address.sin_addr));
            int sd = socket(AF_INET, SOCK_STREAM, 0);
            connect(sd, (struct sockaddr*)&address, sizeof(address));
            usleep(50000);
            close(sd);
            usleep(50000);
            //well, this was easy... and workes, bitches!
        }

        int getTotalNumberOfClients(){
            return _allClients.size();
        }

        int makeSnapshotOfClients(){
            int output = -1;
            _snapshotMutex.lock();
            _clientSnapshotForInformationCollection.clear();
            for( auto const &reference : _allClients ){
                std::pair< Client*, Instrument > temp;
                temp.first = reference.second;
                if( _bateria != NULL ){
                    temp.second = _bateria->getInstrumentOfClientWithChipID( reference.second->getChipID() );
                }else{
                    temp.second = nullpointer;
                }
                _clientSnapshotForInformationCollection.push_back( temp );
            }
            output = _clientSnapshotForInformationCollection.size();
            _snapshotMutex.unlock();
            return output;
        }

        void makeClientWithIDblink( std::string in_chipID, ColorSet in_colorSet ){
            if( _allClients.count( in_chipID ) ){
                _allClients[in_chipID]->addMessageToStack( _messageGenerator->blinkThreeTimesInColor(in_colorSet) );
            }else{
                //print( "ConnectionManager::makeClientWithIDblink(): client not found... in_chipID: " + in_chipID );
            }
        }

        void setAllClientsToWifiForcedSleepWithSeconds( int in_seconds ){
            for( auto const &reference : _allClients ){
                Client* currentClient = reference.second;
                if( currentClient->getIsConnected() ){
                    if(_messageGenerator != NULL){
                        currentClient->addMessageToStack( _messageGenerator->setClientToWifiForcedSleepWithSeconds(in_seconds) );
                    }else{
                        print("_messageGenerator == NULL");
                    }
                }else{
                    print("client not conencted");
                }
            }
        }

        void setAllClientsToDeepSleep( int in_seconds ){
            for( auto const &reference : _allClients ){
                Client* currentClient = reference.second;
                if( currentClient->getIsConnected() ){
                    if(_messageGenerator != NULL){
                        currentClient->addMessageToStack( _messageGenerator->setClientToDeepSleepWithSeconds(in_seconds) );
                    }else{
                        print("_messageGenerator == NULL");
                    }
                }else{
                    print("client not connected");
                }
            }
        }

        bool reset( std::string in_chipID ){
            bool output  = false;
            if( _messageGenerator != NULL ){
                if( _allClients.count( in_chipID ) ){
                    Client *tempClient = _allClients[in_chipID];
                    if (tempClient->getIsConnected()) {
                        tempClient->addMessageToStack( _messageGenerator->reset() );
                        output = true;
                    }
                }
            }else{
                print("_messageGenerator == NULL");
            }
            return output;
        }

        bool setClientToColor( std::string in_chipID, ColorSet in_colorSet ){
            bool output  = false;
            if( _messageGenerator != NULL ){
                if( _allClients.count( in_chipID ) ){
                    Client *tempClient = _allClients[in_chipID];
                    if (tempClient->getIsConnected()) {
                        tempClient->addMessageToStack( _messageGenerator->getFadeToColorInSteps( in_colorSet, 1 ) );
                        output = true;
                    }
                }
            }else{
                print("_messageGenerator == NULL");
            }
            return output;
        }

        Client* getClient( std::string in_chipID ){
            Client* output = NULL;
            if( _allClients.count( in_chipID ) ){
                output = _allClients[in_chipID];
            }else{
                //print("client not found. in_chipID: " + in_chipID);
            }
            return output;
        }

        void sendMessageToClientWithChipID( std::string in_toSendMessage, std::string in_chipID ){
            if( _allClients.count(in_chipID) == true ){
                _allClients[in_chipID]->addMessageToStack( in_toSendMessage );
            }else{
                print("sendMessageToClientWithChipID(): unknown chipID");
            }
        }

    private:

        void initSocketCommunication(){
            _incommingClients_socketFileDescriptor = socket(AF_INET, SOCK_STREAM, 0);          //creates a new socket.
            if (_incommingClients_socketFileDescriptor < 0){
                print("ERROR opening socket");
                _serverRunning = false;
            }
            bzero((char *) &_server_addr, sizeof(_client_addr));     //sets all values in a buffer to zero.

            _server_addr.sin_family = AF_INET;                    //define the domain used.
            _server_addr.sin_addr.s_addr = INADDR_ANY;            //Permit any incoming IP adress by declare INADDR_ANY
            _server_addr.sin_port = htons( PORT );                //unsigned short sin_port, which contain the port number. However, instead of simply copying the port number to this field, it is necessary to convert this to network byte order using the function htons() which converts a port number in host byte order to a port number in network byte order.

            // kill "Address already in use" error message          //http://www.unix.com/programming/29475-how-solve-error-bind-address-already-use.html
            int64_t tr=1;
            if( setsockopt( _incommingClients_socketFileDescriptor, SOL_SOCKET,SO_REUSEADDR, &tr, sizeof(int) ) == -1 ){
                print("setsockopt");
                _serverRunning = false;
            }


            if( bind( _incommingClients_socketFileDescriptor, (struct sockaddr*) &_server_addr, sizeof(_server_addr) ) < 0 ){
                print( "ERROR on binding" );
            }

            listen( _incommingClients_socketFileDescriptor, 5 );    //allows the process to listen on the socket for connections. The first argument is the socket file descriptor, and the second is the size of the backlog queue, i.e., the number of connections that can be waiting while the process is handling a particular connection. This should be set to 5, the maximum size permitted by most systems. If the first argument is a valid socket, this call cannot fail, and so the code doesn't check for errors.             //The listen() man page has more information.

            _sizeOfAdressOfClient = sizeof( _client_addr );             // stores the size of the address of the client. This is needed for the accept system call.

            signal( SIGPIPE, handler );

            usleep( 1000 );
        }

        std::string readDirectlyFromSocket( int64_t in_socketFiledescriptor ){           //get called by startThreadForWaitingForMessages()

            //print(" entering client::readDirectlyFromSocket()");
            std::string Output = "";
            int64_t succesfullyTransmitted = 0;               //return value for the read() and write() calls; i.e. it contains the number of characters read or written.
            char receivedMessage_char[READ_MESSAGE_BUFFER];    //The server reads characters from the socket connection into this buffer.

            bzero(receivedMessage_char,READ_MESSAGE_BUFFER);       //initializes the buffer using the bzero() function,

            char EmptyMessageForCompare[READ_MESSAGE_BUFFER];                   //this is needed some lines below to check, if the reading was successful and

            strcpy(EmptyMessageForCompare, receivedMessage_char);

            //print("ConnectionManager::readDirectlyFromSocket(): trying to read from fd " + to_string(in_socketFiledescriptor));
            try{
                succesfullyTransmitted = read(in_socketFiledescriptor, receivedMessage_char, READ_MESSAGE_BUFFER-1);
            }catch(...){
                print("ConnectionManager::readDirectlyFromSocket(): catching at reading from Socket");
                //markClientAsDisconnected();
            }
            //print( "ConnectionManager::readDirectlyFromSocket(): fd: " + to_string(in_socketFiledescriptor) +  " ; Successfullytransmitted: " + to_string(succesfullyTransmitted) + " ; ReceivedMessage: " + receivedMessage_char );

            if(strcmp(EmptyMessageForCompare, receivedMessage_char) == 0 ){                          //i did this an the if(ClientIsConnected) above to finnally get rid of the problem, that if the first connected client disconnected made the server crash
                print("ConnectionManager::readDirectlyFromSocket(): reading an empty Message from fd " + to_string(in_socketFiledescriptor) + " ... marking it as dissconnected!" );
            }
            if(succesfullyTransmitted == 0 ){
                print("ConnectionManager::readDirectlyFromSocket(): fd: " + to_string(in_socketFiledescriptor) + " disconnected.");
            }
            if ( succesfullyTransmitted < 0 ){                                        // so i copy it to this place but without the exit(1)
                print("ConnectionManager::readDirectlyFromSocket(): ERROR reading from socket");
            }
            Output = to_string(receivedMessage_char);
            //print("leaving ConnectionManager::readDirectlyFromSocket()");
            return Output;
        }

        bool _serverRunning = false;
        int64_t _incommingClients_socketFileDescriptor = -1; // this fd will be used to listen for new clients!
        struct sockaddr_in _server_addr;
        struct sockaddr_in _client_addr;
        socklen_t _sizeOfAdressOfClient;
        std::map< std::string, Client* > _allClients;

};

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_BateriaSetupTab_getInstrumentOnPosition( JNIEnv *env, jobject, int in_row, int in_colomn ) {
#else
    std::string getInstrumentOnPosition( int in_row, int in_colomn ) {
#endif
    std::string output = "";
    if( _bateria != NULL ){
        Position temp(in_row, in_colomn);
        output = _bateria->getInstrumentOnPosition(temp);
    }else{
        print("native getInstrumentOnPosition(): _bateriaVector == NULL");
    }

    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_ServerTab_getCppServerRunning( JNIEnv *env, jobject ){
#else
bool getCppServerRunning(){
#endif
    bool output = false;
    if( _connectionManager != NULL ){
        output = _connectionManager->getServerRunning();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_BateriaSetupTab_getCppServerRunning( JNIEnv *env, jobject ){
    bool output = false;
    if( _connectionManager != NULL ){
        output = _connectionManager->getServerRunning();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_OnMarkedClientsTab_getCppServerRunning( JNIEnv *env, jobject ){
    bool output = false;
    if( _connectionManager != NULL ){
        output = _connectionManager->getServerRunning();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_OnAllClientsTab_getCppServerRunning( JNIEnv *env, jobject ){
    bool output = false;
    if( _connectionManager != NULL ){
        output = _connectionManager->getServerRunning();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_EffectsListTab_getCppServerRunning( JNIEnv *env, jobject ){
    bool output = false;
    if( _connectionManager != NULL ){
        output = _connectionManager->getServerRunning();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_ClientsListTab_getCppServerRunning( JNIEnv *env, jobject ){
    bool output = false;
    if( _connectionManager != NULL ){
        output = _connectionManager->getServerRunning();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_DisplayTab_getCppServerRunning( JNIEnv *env, jobject ){
    bool output = false;
    if( _connectionManager != NULL ){
        output = _connectionManager->getServerRunning();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}
#endif

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_ServerTab_getNumberOfClientsFromCppServer( JNIEnv *env, jobject ){
#else
int getNumberOfClientsFromCppServer(){
#endif
    int output = -1;
    if( _connectionManager != NULL ){
        output = _connectionManager->getNumberOfOnlineClients();
    }else{
        print("_connectionManager == NULL");
    }
    //print("getNumberOfClientsFromCppServer(): output: " + to_string(output));
    return output;
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_StatusTab_getNumberOfOnlineClients( JNIEnv *env, jobject ){
#else
int getNumberOfOnlineClients(){
#endif
    int output = -1;
    if( _connectionManager != NULL ){
        output = _connectionManager->getNumberOfOnlineClients();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_MainActivity_initServer( JNIEnv *env, jobject, jstring in_appPath ){
    std::string appPath = jstring2string( env, in_appPath );
#else
void initServer( string in_appPath ){
std::string appPath = in_appPath;
#endif
    _connectionManager = new ConnectionManager();
    _messageGenerator = new MessageGenerator();
    _globalLog = new GlobalLog( appPath + "/multiversum/" );
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_ServerTab_startCppServer( JNIEnv *env, jobject ){
#else
void startCppServer(){
#endif
    print("entering native startCppServer()");

    if( _connectionManager != NULL ){
        std::thread cppMainThread( &ConnectionManager::waitForClients_Loop, _connectionManager );
        cppMainThread.detach();

        std::thread checkForOfflineClientsThread( &ConnectionManager::checkForOfflineClientsLoop, _connectionManager );
        checkForOfflineClientsThread.detach();

        while( _connectionManager->getServerRunning() != false ){
            print( "native ServerTab::startCppServer(): waiting for getServerRunning() == true" );
            usleep(1000000);
        }
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_ServerTab_stopCppServer( JNIEnv *env, jobject ){
#else
void stopCppServer(){
#endif

    if( _globalLog != NULL ){
        //_globalLog->closeCurrentFile();
        _globalLog->~GlobalLog();
        _globalLog = NULL;
    }

    if( _connectionManager != NULL ){
        _connectionManager->shutdown();
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_ServerTab_makeThemBlinkWhite( JNIEnv *env, jobject ){
#else
void makeThemBlinkWhite(){
#endif
    if( _connectionManager != NULL ){
        if( _messageGenerator != NULL ){
            _connectionManager->sendMessageToAllClients(
                    //_messageGenerator->blinkThreeTimesInColor(_messageGenerator->WHITE) );
                    _messageGenerator->blinkThreeTimesInColor( _messageGenerator->DARK_GREEN ) );
        }else{
            print("_messageGenerator == NULL");
        }
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_blinkOnPositionWithColor( JNIEnv *env, jobject, jint in_row, jint in_colomn, jint in_red, jint in_green, jint in_blue, jint in_uv ){
#else
void blinkOnPositionWithColor( int in_row, int in_colomn, int in_red, int in_green, int in_blue, int in_uv ){
#endif
    if( _bateria != NULL ){
        if( _messageGenerator != NULL ){
            ColorSet temp_color( in_red, in_green, in_blue, in_uv );
            std::string toSendMessage = _messageGenerator->blinkThreeTimesInColor(temp_color);
            Position temp_Position( in_row, in_colomn );
            _bateria->sendFadeSeriesToClientOnPosition( temp_Position, toSendMessage );
        }else{
            print("_messageGenerator == NULL");
        }
    }else{
        print("native blinkOnPositionWithColor(): _bateriaVector == NULL");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_initBateria( JNIEnv *env, jobject, jint in_rows, jint in_colomns ){
#else
void initBateria( int in_rows, int in_colomns ){
#endif
    if( _bateria == NULL ){
        _bateria = new Bateria( in_rows, in_colomns );
    }else{
        print("_bateriaVector != NULL");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_learnPosition( JNIEnv *env, jobject, jint in_row, jint in_colomn ){
#else
void learnPosition( int in_row, int in_colomn ){
#endif
    if(_bateria != NULL) {
        Position temp(in_row, in_colomn);
        _bateria->learnPosition(temp);
    }else{
        print("Java_multiversum_multiversum_BateriaTab_learnPosition(): _bateriaVector not initiated!!!!");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_clearLearnPosition( JNIEnv *env, jobject ){
#else
void clearLearnPosition(){
#endif
    if(_bateria != NULL) {
        _bateria->clearLearnPosition();
    }else{
        print("Java_multiversum_multiversum_BateriaTab_clearLearnPosition(): _bateriaVector not initiated!!!!");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_BateriaSetupTab_setInstrumentOnPosition( JNIEnv *env, jobject jobj, jstring in_instrument, jint in_row, jint in_colomn ){
    std::string cppString_instrument = jstring2string( env, in_instrument );
#else
bool setInstrumentOnPosition( string in_instrument, int in_row, int in_colomn ){
    std::string cppString_instrument = in_instrument;
#endif
    bool output = false;
    if(_bateria != NULL){
        Instrument setToInstrument = getInstrumentOfString( cppString_instrument );
        Position setToPosition(in_row, in_colomn);
        output = _bateria->setInstrumentOnPosition(setToPosition, setToInstrument);
    }else{
        print("Java_multiversum_multiversum_BateriaTab_setInstrumentOnPosition(): _bateriaVector not initiated!!!!");
    }
    return output;
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_addToIsMarkedForActions( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
void addToIsMarkedForActions( int in_row, int in_colomn ){
#endif
    Position temp(in_row, in_colomn);
    if( _bateria != NULL ){
        _bateria->addClientOnPositionToMarkedForAction(temp);
    }else{
        print("native addToIsMarkedForActions(): _bateriaVector == NULL");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_removeFromIsMarkedForActions( JNIEnv *env, jobject jobj, jint in_row, jint in_colomn ){
#else
void removeFromIsMarkedForActions( int in_row, int in_colomn ){
#endif
    if( _bateria != NULL ){
        Position temp(in_row, in_colomn);
        _bateria->removeClientOnPositionFromMarkedForAction( temp );
    }else{
        print("native removeFromIsMarkedForActions(): _bateriaVector == NULL");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_BateriaSetupTab_getRowsOfBateria( JNIEnv *env, jobject jobj ){
#else
int getRowsOfBateria(){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getRows();
    }else{
        print("_bateria == NULL");
    }
    return output;
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_BateriaSetupTab_getColomnsOfBateria( JNIEnv *env, jobject jobj ){
#else
int getColomnsOfBateria(){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getColomns();
    }else{
        print("_bateria == NULL");
    }
    return output;
}

/*
#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_setMarkedClientsToColor( JNIEnv *env, jobject jobj, int in_red, int in_green, int in_blue, int in_uv ){
#else
void setMarkedClientsToColor( int in_red, int in_green, int in_blue, int in_uv ){
#endif

    if( _bateria != NULL ) {
        ColorSet temp(in_red, in_green, in_blue, in_uv);
        //temp.red = in_red;
        //temp.green = in_green;
        //temp.blue = in_blue;
        _bateria->setMarkedClientsToColor(temp);
    }else{
        print("_bateriaVector == NULL");
    }

}
 */


#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_OnMarkedClientsTab_setMarkedClientsToColor( JNIEnv *env, jobject jobj, int in_red, int in_green, int in_blue, int in_uv ){
#else
bool setMarkedClientsToColor( int in_red, int in_green, int in_blue, int in_uv ){
#endif

    bool output = false;
    if( _bateria != NULL ){
        ColorSet temp(in_red, in_green, in_blue, in_uv);
        output = _bateria->setMarkedClientsToColor( temp );
    }else{
        print("_bateria == NULL");
    }
    return output;
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_ServerTab_setAllClientsToColor( JNIEnv *env, jobject jobj, int in_red, int in_green, int in_blue, int in_uv ) {
#else
void setAllClientsToColor( int in_red, int in_green, int in_blue, int in_uv ){
#endif
    if( _connectionManager != NULL ){
        if( _messageGenerator != NULL ){
            ColorSet temp_color(in_red, in_green, in_blue, in_uv);
            _connectionManager->sendMessageToAllClients( _messageGenerator->getFadeToColorInSteps( temp_color, 1 ) );
        }else{
            print("_messageGenerator == NULL");
        }
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_BateriaSetupTab_getSomeClientToInstrumentAssignmentHasChanged( JNIEnv *env, jobject jobj ){
#else
bool getSomeClientAssignmentHasChanged(){
#endif
    bool output = false;
    if( _bateria != NULL ){
        output = _bateria->getSomeClientToInstrumentAssignmentHasChanged();
    }else{
        //print("natvie getSomeClientAssignmentHasChanged(): _bateriaVector == NULL");
    }
    return output;
}

#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_ClientsListTab_collectClientsInformation( JNIEnv *env, jobject jobj ){
#else
int collectClientsInformation(){
#endif
    int output = -1;
    if( _connectionManager != NULL ){
        output = _connectionManager->makeSnapshotOfClients();    //collects a pointers of the current clients and puts them (and the instrument-information) in a vector
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

#ifdef RUN_ON_ANDROID   //we have two of them
#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_BateriaSetupTab_collectClientsInformation( JNIEnv *env, jobject jobj ){
#else
int collectClientsInformation(){
#endif
    int output = -1;
    if( _connectionManager != NULL ){
        output = _connectionManager->makeSnapshotOfClients();    //collects a pointers of the current clients and puts them (and the instrument-information) in a vector
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}
#endif

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getChipIDFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getChipIDfromSnapshotOnPosition( int in_position ){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    //print( "_clientSnapshotForInformationCollection.size(): " + to_string( _clientSnapshotForInformationCollection.size() ) );
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if( (_clientSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _clientSnapshotForInformationCollection[in_position].first->getChipID();
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef RUN_ON_ANDROID
#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_BateriaSetupTab_getChipIDFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getChipIDfromSnapshotOnPosition( int in_position ){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    print( "_clientSnapshotForInformationCollection.size(): " + to_string( _clientSnapshotForInformationCollection.size() ) );
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if( (_clientSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _clientSnapshotForInformationCollection[in_position].first->getChipID();
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}
#endif

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getVersionFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getVersionfromSnapshotOnPosition(int in_position){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if ((_clientSnapshotForInformationCollection.size() - 1) >= in_position) {
            output = _clientSnapshotForInformationCollection[in_position].first->getVersion();
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getLastSeenFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getLastSeenfromSnapshotOnPosition(int in_position){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if( (_clientSnapshotForInformationCollection.size() - 1 ) >= in_position){
            output = _clientSnapshotForInformationCollection[in_position].first->getLastSeen();
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef  RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getChainLengthFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getChainLengthFromSnapshotOnPosition( int in_position ){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if( (_clientSnapshotForInformationCollection.size() - 1 ) >= in_position ){
            output = _clientSnapshotForInformationCollection[in_position].first->getChainLength();
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef  RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getSignalStrengthFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getSignalStrengthfromSnapshotOnPosition(int in_position){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if ((_clientSnapshotForInformationCollection.size() - 1) >= in_position) {
            output = _clientSnapshotForInformationCollection[in_position].first->getSignalStrength();
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getConnectedFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getConnectedFromSnapshotOnPosition(int in_position){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if ((_clientSnapshotForInformationCollection.size() - 1) >= in_position) {

            if (_clientSnapshotForInformationCollection[in_position].first->getIsConnected()) {
                output = "connected";
            } else {
                output = "not connected";
            }
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getInstrumentFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getInstrumentFromSnapshotOnPosition(int in_position){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if ((_clientSnapshotForInformationCollection.size() - 1) >= in_position) {
            Instrument instrument = _clientSnapshotForInformationCollection[in_position].second;
            if (instrument == unknown) {
                output = "unknown";
            } else if (instrument == mestre) {
                output = "mestre";
            } else if (instrument == caixa) {
                output = "caixa";
            } else if (instrument == tamborim) {
                output = "tamborim";
            } else if (instrument == repique) {
                output = "repique";
            } else if (instrument == chocalho) {
                output = "chocalho";
            } else if (instrument == primera) {
                output = "primera";
            } else if (instrument == segunda) {
                output = "segunda";
            } else if (instrument == tetiera) {
                output = "tetiera";
            } else if (instrument == agogo) {
                output = "agogo";
            } else if (instrument == nullpointer) {
                output = "null";
            } else {
                print("getInstrumentFromSnapshotOnPosition(): invalid Instrument-string");
                output = "invalid instrument";
            }
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getIpAdressFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getIpAdressFromSnapshotOnPosition(int in_position){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if ((_clientSnapshotForInformationCollection.size() - 1) >= in_position) {
            output = _clientSnapshotForInformationCollection[in_position].first->getIpAdressAsString();
        } else {
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getLastStateFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getLastStateFromSnapshotOnPosition(int in_position){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if( (_clientSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _clientSnapshotForInformationCollection[in_position].first->getLastState();
        }else{
            print( "invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_ClientsListTab_getEffectFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getEffectFromSnapshotOnPosition(int in_position){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _clientSnapshotForInformationCollection.size() != 0 ){
        if( ( _clientSnapshotForInformationCollection.size() - 1 ) >= in_position ){
            output = _clientSnapshotForInformationCollection[in_position].first->getEffectName();
        }else{
            print("invalid position on _clientSnapshotForInformationCollection ... in_position: " + to_string(in_position));
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jboolean JNICALL Java_multiversum_multiversum_BateriaSetupTab_getIsMarked( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
bool getIsMarked( int in_row, int in_colomn ){
#endif
    bool output = false;
    if( _bateria != NULL ){
        output = _bateria->getIsMarked( in_row, in_colomn );
    }else{
        print("getIsMarked: _bateria == NULL");
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_makeClientWithIDblink( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
void makeClientWithIDblink( string in_chipID ){
    std::string in_CppString = in_chipID;
#endif
    if( _connectionManager != NULL ){
        if( _messageGenerator != NULL ){
            _connectionManager->makeClientWithIDblink(in_CppString, _messageGenerator->RED);
        }else{
            print("_messageGenerator == NULL");
        }
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef RUN_ON_ANDROID
#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterEffectsList_makeClientWithIDblink( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
    void makeClientWithIDblink( string in_chipID ){
    std::string in_CppString = in_chipID;
#endif
    if( _connectionManager != NULL ){
        if( _messageGenerator != NULL ){
            _connectionManager->makeClientWithIDblink(in_CppString, _messageGenerator->RED);
        }else{
            print("_messageGenerator == NULL");
        }
    }else{
        print("_connectionManager == NULL");
    }
}
#endif

#ifdef RUN_ON_ANDROID
#ifdef RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_makeClientWithIDblink( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
void makeClientWithIDblink( string in_chipID ){
    std::string in_CppString = in_chipID;
#endif
    if( _connectionManager != NULL ){
        if( _messageGenerator != NULL ){
            _connectionManager->makeClientWithIDblink(in_CppString, _messageGenerator->RED);
        }else{
            print("_messageGenerator == NULL");
        }
    }else{
        print("_connectionManager == NULL");
    }
}
#endif

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_ClientsListTab_getTotalNumberOfClients( JNIEnv *env, jobject jobj ){
#else
int getTotalNumberOfClients(){
#endif
    int output = -1;
    if( _connectionManager != NULL ){
        output = _connectionManager->getTotalNumberOfClients();
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_OnAllClientsTab_startGameOfLife( JNIEnv *env, jobject jobj ){
#else
void startGameOfLife(){
#endif
    print("game of life not implemented yet");
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_ServerTab_sleepAllClientsForSeconds( JNIEnv *env, jobject jobj, int in_seconds ){
#else
void sleepAllClientsForSeconds( int in_seconds ){
#endif
    if( _connectionManager != NULL && _messageGenerator != NULL ){
        _connectionManager->sendMessageToAllClients( _messageGenerator->sleepForSeconds(in_seconds) );
    }else{
        print("_connectionManager == NULL || _messageGenerator == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_ServerTab_clientsShouldMakeSignals( JNIEnv *env, jobject jobj, bool in_shouldMakeSignals ){
#else
void clientsShouldMakeSignals( bool in_shouldMakeSignals ){
#endif
    if(  _connectionManager != NULL && _messageGenerator != NULL ){
        _connectionManager->sendMessageToAllClients( _messageGenerator->shouldMakeSignals(in_shouldMakeSignals) );
    }else{
        print("_connectionManager == NULL || _messageGenerator == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_ServerTab_setAllClientsToWifiForcedSleepWithSeconds( JNIEnv *env, jobject jobj, int in_seconds ){
#else
void setAllClientsToWifiForcedSleepWithSeconds( int in_seconds ){
#endif
    if( _connectionManager != NULL ){
        _connectionManager->setAllClientsToWifiForcedSleepWithSeconds( in_seconds );
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_ServerTab_setAllClientsToDeepSleep( JNIEnv *env, jobject jobj, int in_seconds ){
#else
void setAllClientsToDeepSleep( int in_seconds ){
#endif
    if( _connectionManager != NULL ){
        _connectionManager->setAllClientsToDeepSleep( in_seconds );
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_setInstrumentOnFromChipID( JNIEnv *env, jobject jobj, jstring in_chipID, jstring in_instrument ){
    std::string chipID = jstring2string( env, in_chipID );
    std::string instrument_string = jstring2string( env, in_instrument );
#else
bool setInstrumentOnFromChipID( string in_chipID, string in_instrument ){
    std::string chipID = in_chipID;
    std::string instrument_string = in_instrument;
#endif
    bool output = false;
    if( _bateria != NULL ){
        Instrument instrument = getInstrumentOfString( instrument_string );
        output = _bateria->setInstrumentOfChipID( chipID, instrument );
    }else{
        print("_bateria == NULL");
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_reset( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
bool reset( string in_chipID ){
    std::string in_CppString  = in_chipID;
#endif
    bool output = false;
    if( _connectionManager != NULL ){
        output = _connectionManager->reset( in_CppString );
    }else{
        print("_connecitionManager == NULL");
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_setClientToColor( JNIEnv *env, jobject jobj, jstring in_chipID, int in_red, int in_green, int in_blue, int in_uv ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
bool setMarkedClientsToColor( string in_chipID, int in_red, int in_green, int in_blue, int in_uv ){
    std::string in_CppString  = in_chipID;
#endif
    bool output = false;
    if( _connectionManager != NULL ){
        ColorSet tempColorSet(in_red, in_green, in_blue, in_uv);
        output = _connectionManager->setClientToColor( in_CppString, tempColorSet );
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_lockLogOnClient( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
void lockLogOnClient( string in_chipID ){
    std::string in_CppString  = in_chipID;
#endif
    if( _connectionManager != NULL ){
        Client* client = _connectionManager->getClient( in_CppString );
        if( client != NULL ){
            Log* log = client->getLog();
            if( log != NULL ) {
                log->lockLog();
            }else{
                print("log not found!");
            }
        }else{
            print( "client not found!" );
        }
    }else {
        print("_connectionManager == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_getLogSize( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
int getLogSize( string in_chipID ){
    std::string in_CppString  = in_chipID;
#endif
    int output = -1;
    if( _connectionManager != NULL ){
        Client* client = _connectionManager->getClient( in_CppString );
        if( client != NULL ){
            Log* log = client->getLog();
            if( log != NULL ) {
                output = log->getSize();
            }else{
                print("log not found!");
            }
        }else{
            print( "client not found!" );
        }
    }else{
        print("_connectionManager == NULL");
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_prepareRead( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
void prepareRead( string in_chipID ){
    std::string in_CppString  = in_chipID;
#endif
    if( _connectionManager != NULL ){
        Client* client = _connectionManager->getClient( in_CppString );
        if( client != NULL ){
            Log* log = client->getLog();
            if( log != NULL ) {
                log->prepareForRead();
            }else{
                print("log not found!");
            }
        }else{
            print( "client not found!" );
        }
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_getNextLogLine( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
string getNextLogLine( string in_chipID ){
    string in_CppString  = in_chipID;
#endif
    std::string output = "";
    if( _connectionManager != NULL ){
        Client* client = _connectionManager->getClient( in_CppString );
        if( client != NULL ){
            Log* log = client->getLog();
            if( log != NULL ) {
                output = log->getNextLine();
            }else{
                print("log not found!");
            }
        }else{
            print( "client not found!" );
        }
    }else{
        print("_connectionManager == NULL");
    }
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF(output.c_str());
    #else
    return output;
    #endif

}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_unlockLog( JNIEnv *env, jobject jobj, jstring in_chipID ){
    std::string in_CppString = jstring2string( env, in_chipID );
#else
void unlockLog( string in_chipID ){
    std::string in_CppString  = in_chipID;
#endif

    if( _connectionManager != NULL ){
        Client* client = _connectionManager->getClient( in_CppString );
        if( client != NULL ){

            Log* log = client->getLog();
            if( log != NULL ){
                log->unlockLog();
            }else{
                print("log not found");
            }

        }else{
            print( "client not found!" );
        }
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_setWaterDropEffectOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn, int in_red, int in_green, int in_blue, int in_uv, int in_triggerThreshold, int in_speed, bool in_stayOnColor, int in_attack, int in_release, bool in_randomColor ){
#else
bool setWaterDropEffectOnPosition( int in_row, int in_colomn, int in_red, int in_green, int in_blue, int in_uv, int in_triggerThreshold, int in_speed, bool in_stayOnColor, int in_attack, int in_release, bool in_randomColor ){
#endif
    //print("entering native setWaterDropEffectOnPosition()");
    bool output = false;

    if( _bateria != NULL ){
        if( in_randomColor == false ){
            ColorSet tempColor( in_red, in_green, in_blue, in_uv );
            Position tempPosition(in_row, in_colomn);
            print("setting WaterDropEffect on position: " + to_string( in_row ) + ";" + to_string(in_colomn) );
            output = _bateria->setWaterDropEffectOnPosition( tempPosition, tempColor, in_triggerThreshold, in_speed, in_stayOnColor, in_attack, in_release );
        }else{
            ColorSet tempColor( in_randomColor, in_randomColor, in_randomColor, in_randomColor );
            Position tempPosition(in_row, in_colomn);
            output = _bateria->setWaterDropEffectOnPosition( tempPosition, tempColor, in_triggerThreshold, in_speed, in_stayOnColor, in_attack, in_release );
        }
    }else{
        print("_bateria == NULL");
    }
    //print("leaving native setWaterDropEffectOnPosition()");
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_editWaterDropEffectOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn, int in_red, int in_green, int in_blue, int in_uv, int in_speed, bool in_stayOnColor, int in_attack, int in_release, bool in_randomColor ){
#else
bool editWaterDropEffectOnPosition( int in_row, int in_colomn, int in_red, int in_green, int in_blue, int in_uv, int in_speed, bool in_stayOnColor, int in_attack, int in_release, bool in_randomColor ){
#endif
    //print("entering native editWaterDropEffectOnPosition()");
    bool output = false;
    if( _bateria != NULL ){
        if( in_randomColor == false ){
            ColorSet tempColor( in_red, in_green, in_blue, in_uv );
            Position tempPosition( in_row, in_colomn );
            output = _bateria->editWaterDropEffectOnPosition( tempPosition, tempColor, in_speed, in_stayOnColor, in_attack, in_release );
        }else{
            ColorSet tempColor( in_randomColor, in_randomColor, in_randomColor, in_randomColor );
            Position tempPosition( in_row, in_colomn );
            output = _bateria->editWaterDropEffectOnPosition( tempPosition, tempColor, in_speed, in_stayOnColor, in_attack, in_release );
        }
    }else{
        print("_bateria == NULL");
    }
    //print("leaving native editWaterDropEffectOnPosition()");
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_setWaterDropEffectOnChip( JNIEnv *env, jobject jobj, jstring in_chipID, int in_red, int in_green, int in_blue, int in_uv, int in_triggerThreshold, int in_speed, bool in_stayOnColor, int in_attack, int in_release, bool in_randomColor ){
    std::string cppString = jstring2string( env, in_chipID );
#else
bool setWaterDropEffectOnChip( string in_chipID, int in_red, int in_green, int in_blue, int in_uv, int in_triggerThreshold, int in_speed, bool in_stayOnColor, int in_attack, int in_release, bool in_randomColor ){
    std::string cppString = in_chipID;
#endif
    bool output = false;
    if( _bateria != NULL ){
        if( in_randomColor == false ) {
            ColorSet tempColor(in_red, in_green, in_blue, in_uv);
            output = _bateria->setWaterDropEffectOnChip( cppString, tempColor, in_triggerThreshold, in_speed, in_stayOnColor, in_attack, in_release );
        }else{
            ColorSet tempColor( in_randomColor, in_randomColor, in_randomColor, in_randomColor );
            output = _bateria->setWaterDropEffectOnChip( cppString, tempColor, in_triggerThreshold, in_speed, in_stayOnColor, in_attack, in_release );
        }
    }else{
        print("_bateria == NULL");
    }
    return output;
}


/*
#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_bateriaExists( JNIEnv *env, jobject jobj ){
#else
bool bateriaExists(){
#endif
    bool output = false;
    if( _bateria != NULL ){
        output = true;
    }
    return output;
}
 */

#ifdef RUN_ON_ANDROID   // we have two of them
#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_bateriaExists( JNIEnv *env, jobject jobj ){
#else
bool bateriaExists(){
#endif

    bool output = false;
    if( _bateria != NULL ){
        output = true;
    }
    return output;

}
#endif

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_BateriaSetupTab_assignClientWithIDtoPosition( JNIEnv *env, jobject jobj, jstring in_chipID, int in_row, int in_colomn ){
    std::string cppString = jstring2string( env, in_chipID );
#else
void assignClientWithIDtoPosition( string in_chipID, int in_row, int in_colomn ){
    std::string cppString = in_chipID;
#endif
    if( _bateria != NULL ){
        if( _connectionManager != NULL){
            Position temp(in_row, in_colomn);
            Client* client = _connectionManager->getClient(cppString);
            _bateria->assignClientToPosition(client, temp);
        }else{
            print("_connectionManager == NULL");
        }
    }else{
        print("_bateria == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_sendStringDirectlyToClientWithID( JNIEnv *env, jobject jobj, jstring in_toSendString, jstring in_chipID ){
    std::string cppToSendString = jstring2string( env, in_toSendString );
    std::string cppChipID = jstring2string( env, in_chipID);
#else
void sendStringDirectlyToClientWithID( string in_toSendString, string in_chipID ){
    std::string cppToSendString = in_toSendString;
    std::string cppChipID =    in_chipID;
#endif
    if( _connectionManager != NULL ){
        _connectionManager->sendMessageToClientWithChipID( cppToSendString, cppChipID );
    }else{
        print("_connectionManager == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_BateriaSetupTab_getHasActiveEffect( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
bool getHasActiveEffect( int in_row, int in_colomn ){
#endif
    bool output = false;
    if( _bateria != NULL){
        output = _bateria->getHasActiveEffect(in_row, in_colomn);
    }else{
        print("_bateria == NULL");
    }
    //print("native getHasActiveEffect(): in_row: " + to_string(in_row) + " in_colomn: " + to_string(in_colomn) + " ... output: " + to_string(output));
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_BateriaSetupTab_getHasEffect( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
bool getHasEffect( int in_row, int in_colomn ){
#endif
    bool output = false;
    if( _bateria != NULL){
        output = _bateria->getHasEffect(in_row, in_colomn);
    }else{
        print("_bateria == NULL");
    }
    //print("native getHasEffect(): in_row: " + to_string(in_row) + " in_colomn: " + to_string(in_colomn) + " ... output: " + to_string(output));
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_BateriaSetupTab_getHasClientSetOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
bool getHasClientSetOnPosition( int in_row, int in_colomn ){
#endif
    bool output = false;
    if( _bateria != NULL){
            output = _bateria->getHasClientSetOnPosition( in_row, in_colomn );
    }else{
        print( "_bateria == NULL" );
    }
    //print("native getHasEffect(): in_row: " + to_string(in_row) + " in_colomn: " + to_string(in_colomn) + " ... output: " + to_string(output));
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_BateriaSetupTab_setEffectOnPositionToState( JNIEnv *env, jobject jobj, int in_row, int in_colomn, bool in_state ){
#else
bool setEffectOnPositionToState( int in_row, int in_colomn, bool in_state ){
#endif
    bool output = false;
    if( _bateria != NULL ){
        output = _bateria->setEffectOnPositionToState( in_row, in_colomn, in_state );
    }else{
        print("native setEffectOnPositionToState(): _bateria == NULL");
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int  JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getRedValueOfEffectOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
int getRedValueOfEffectOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getRedValueOfEffectOnPosition( in_row, in_colomn );
    }else{
        print( "native getRedValueOfEffectOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int  JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getGreenValueOfEffectOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
int getGreenValueOfEffectOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getGreenValueOfEffectOnPosition( in_row, in_colomn );
    }else{
        print( "native getRedValueOfEffectOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int  JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getBlueValueOfEffectOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
int getBlueValueOfEffectOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getBlueValueOfEffectOnPosition( in_row, in_colomn );
    }else{
        print( "native getRedValueOfEffectOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int  JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getUVValueOfEffectOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
int getUVValueOfEffectOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getUVValueOfEffectOnPosition( in_row, in_colomn );
    }else{
        print( "native getRedValueOfEffectOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getEffectHasRandomColorOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
bool getEffectHasRandomColorOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getEffectHasRandomColorOnPosition( in_row, in_colomn );
    }else{
        print( "native getRedValueOfEffectOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getEffectSpeedOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
int getEffectSpeedOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getEffectSpeedOnPosition( in_row, in_colomn );
    }else{
        print( "native getRedValueOfEffectOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getEffectStayOnColorOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
bool getEffectStayOnColorOnPosition( int in_row, int in_colomn ){
#endif
    bool output = false;
    if( _bateria != NULL ){
        output = _bateria->getEffectStayOnColorOnPosition( in_row, in_colomn );
    }else{
        print( "native getEffectStayOnColorOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getEffectAttackOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
int getEffectAttackOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getEffectAttackOnPosition( in_row, in_colomn );
    }else{
        print( "native getEffectAttackOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getEffectReleaseOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
int getEffectReleaseOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getEffectReleaseOnPosition( in_row, in_colomn );
    }else{
        print( "native getEffectAttackOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterClientsList_setLEDchainLength( JNIEnv *env, jobject jobj, int in_newChainLength, jstring in_chipID ){
    std::string cppString = jstring2string( env, in_chipID );
#else
void setLEDchainLength( int in_newChainLength, string in_chipID ){
    std::string cppString =  in_chipID;
#endif
    if( _bateria != NULL ){
        _bateria->setLEDchainLength( in_newChainLength, cppString );
    }else{
        print( "native getEffectAttackOnPosition(): _bateria == NULL" );
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_setTriggerThresholdOnEffectOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn, int in_newTriggerThreshold ){
#else
int setTriggerThresholdOnEffectOnPosition( int in_row, int in_colomn, int in_newTriggerThreshold ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->setTriggerThresholdOnEffectOnPosition( in_row, in_colomn, in_newTriggerThreshold );
    }else{
        print( "native setTriggerThresholdOnEffectOnPosition(): _bateria == NULL" );
    }
    return output;
}
/*
#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_setTriggerThresholdOnEffectOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn, int in_newTriggerThreshold ){
#else
int setTriggerThresholdOnEffectOnPosition( int in_row, int in_colomn, int in_newTriggerThreshold ){
#endif
    if( _bateria != NULL ){
        _bateria->setTriggerThresholdOnEffectOnPosition( in_row, in_colomn, in_newTriggerThreshold );
    }else{
        print( "native setTriggerThresholdOnEffectOnPosition(): _bateria == NULL" );
    }
}
 */

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_WaterDropEffectSetup_getTriggerThresholdOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
int getTriggerThresholdOnPosition( int in_row, int in_colomn ){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getTriggerThresholdOnPosition( in_row, in_colomn );
    }else{
        print( "native getTriggerThresholdOnPosition(): _bateria == NULL" );
    }
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getTotalNumberOfEffects( JNIEnv *env, jobject jobj ){
#else
int getTotalNumberOfEffects(){
#endif

    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->getTotalNumberOfEffects();
    }else{
        print( "native getTotalNumberOfEffects(): _bateria == NULL" );
    }
    return output;

}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_collectEffectsInformation( JNIEnv *env, jobject jobj ){
#else
int collectEffectsInformation(){
#endif
    int output = -1;
    if( _bateria != NULL ){
        output = _bateria->makeSnapshotOfEffects();    //collects a pointers of the current clients and puts them (and the instrument-information) in a vector
    }else{
        print("native collectEffectsInformation(): _bateria == NULL");
    }
    return output;

}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterEffectsList_setEffectTriggerThresholdOnClientOnPositionToValue( JNIEnv *env, jobject jobj, int in_row, int in_colomn, int in_triggerThreshold ){
#else
void setEffectTriggerThresholdOnClientOnPositionToValue( int in_row, int in_colomn, int in_triggerThreshold ){
#endif
    if( _bateria != NULL ){
        _bateria->setEffectTriggerThresholdOnClientOnPositionToValue( in_row, in_colomn, in_triggerThreshold );
    }else{
        print("native setEffectTriggerThresholdOnClientOnPositionToValue(): _bateria == NULL");
    }
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT void JNICALL Java_multiversum_multiversum_CustomAdapterEffectsList_manuallyTriggerEffectOnClientOnPosition( JNIEnv *env, jobject jobj, int in_row, int in_colomn ){
#else
void manuallyTriggerEffectOnClientOnPosition( int in_row, int in_colomn ){
#endif

    if( _bateria != NULL ){
        _bateria->manuallyTriggerEffectOnClientOnPosition( in_row, in_colomn );
    }else{
        print("native manuallyTriggerEffectOnClientOnPosition(): _bateria == NULL");
    }

}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_EffectsListTab_getEffectnameOfEffectFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getEffectnameOfEffectOnPosition( int in_position ){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getEffectName();
        }else{
            print( "invalid position on _effectSnapshotForInformationCollection() ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF( output.c_str() );
    #else
    return output;
    #endif
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getEffectColorRedFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getEffectColorRedFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getColorOfEffect().red;
        }else{
            print( "invalid position on getEffectColor_red_fromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getEffectColorGreenFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getEffectColorGreenFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if (_effectSnapshotForInformationCollection.size() != 0) {
        if ((_effectSnapshotForInformationCollection.size() - 1) >= in_position) {
            output = _effectSnapshotForInformationCollection[in_position]->getColorOfEffect().green;
        } else {
            print("invalid position on getEffectColor_green_fromSnapshotOnPosition() ... in_position: " +
                  to_string(in_position));
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getEffectColorBlueFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getEffectColorBlueFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getColorOfEffect().blue;
        }else{
            print( "invalid position on getEffectColor_blue_fromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getEffectColorUvFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getEffectColorUvFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getColorOfEffect().uv;
        }else{
            print( "invalid position on getEffectColor_blue_fromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT bool JNICALL Java_multiversum_multiversum_EffectsListTab_getStayOnColorValueFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
bool getStayOnColorValueFromSnapshotOnPosition( int in_position ){
#endif
    bool output = false;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getEffectStayOnColor();
        }else{
            print( "invalid position on getEffectColor_blue_fromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getCurrentTriggerThresholdFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getCurrentTriggerThresholdFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getTriggerThreshold();
        }else{
            print( "invalid position on getCurrentTriggerThresholdFromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getSpeedFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getSpeedFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getEffectSpeed();
        }else{
            print( "invalid position on getSpeedFromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getAttackFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getAttackFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getEffectAttack();
        }else{
            print( "invalid position on getAttackFromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getReleaseFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getReleaseFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->getEffectRelease();
        }else{
            print( "invalid position on getReleaseFromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getStateOfEffectFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getStateOfEffectFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            output = _effectSnapshotForInformationCollection[in_position]->isActive();
        }else{
            print( "invalid position on getStateOfEffectFromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getRowFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getRowFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            WaterDrop* PointerInSnapshot = _effectSnapshotForInformationCollection[in_position];
            if( _bateria != NULL ){
                output = _bateria->getPositionOfClientWithThisEffect( PointerInSnapshot ).row;
                if( output == -1){
                    print("native getRowFromSnapshotOnPosition(): Effect not found... in_position: " + to_string(in_position));
                }
            }else{
                print("native getRowFromSnapshotOnPosition(): _bateria == NULL");
            }
        }else{
            print( "invalid position on getRowFromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT int JNICALL Java_multiversum_multiversum_EffectsListTab_getColomnFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
int getColomnFromSnapshotOnPosition( int in_position ){
#endif
    int output = -1;
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            WaterDrop* PointerInSnapshot = _effectSnapshotForInformationCollection[in_position];
            if( _bateria != NULL ){
                output = _bateria->getPositionOfClientWithThisEffect( PointerInSnapshot ).colomn;
                if( output == -1){
                    print("native getColomnFromSnapshotOnPosition(): Effect not found... in_position: " + to_string(in_position));
                }
            }else{
                print("native getColomnFromSnapshotOnPosition(): _bateria == NULL");
            }
        }else{
            print( "invalid position on getColomnFromSnapshotOnPosition() ... in_position: " + to_string(in_position) );
        }
    }
    _snapshotMutex.unlock();
    return output;
}

#ifdef  RUN_ON_ANDROID
extern "C" JNIEXPORT jstring JNICALL Java_multiversum_multiversum_EffectsListTab_getChipIDOfClientWithEffectFromSnapshotOnPosition( JNIEnv *env, jobject jobj, int in_position ){
#else
    std::string getChipIDOfClientWithEffectFromSnapshotOnPosition( int in_position ){
#endif
    std::string output = "";
    _snapshotMutex.lock();
    if( _effectSnapshotForInformationCollection.size() != 0 ){
        if( (_effectSnapshotForInformationCollection.size() - 1) >= in_position ){
            WaterDrop* PointerInSnapshot = _effectSnapshotForInformationCollection[in_position];
            if( _bateria != NULL ){
                output = _bateria->getChipIDOfClientWithThisEffect( PointerInSnapshot );
                if( stringCompare( output, "" ) ){
                    print("native getChipIDOfClientWithEffectFromSnapshotOnPosition(): Effect not found... in_position: " + to_string(in_position));
                }
            }else{
                print("native getChipIDOfClientWithEffectFromSnapshotOnPosition(): _bateria == NULL");
            }
        }else{
            print( "invalid position on _effectSnapshotForInformationCollection() ... in_position: " + to_string(in_position) );
            output = "invalid position";
        }
    }
    _snapshotMutex.unlock();
    #ifdef RUN_ON_ANDROID
    return env->NewStringUTF( output.c_str() );
    #else
    return output;
    #endif
}






