package multiversum.multiversum;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

public class ServerTab extends android.support.v4.app.Fragment {

    //ServerJavaMirror _server_javaMirror = null;
    Button _startServerButton = null;
    TextView _serverStatusTextView = null;
    LinearLayout _contentLinearLayout = null;
    Button _blinkButton = null;
    TextView _numberActiveClientsTextView = null;
    boolean _ui_updateRunnableActive = false;
    boolean _updateUI_inLoop = false;
    boolean _justCreatedView = false;

    int _numberOfClients_old = 0;
    int _numberOfClients = 0;
    boolean _serverRunning_old = false;
    boolean _serverRunning = false;

    Toast _currentToast = null;

    Switch _makeSignalsSwitch = null;
    Switch _deepSleepSwitch = null;
    Switch _wifiForcedSleepSwitch = null;

    Button _sleepButton = null;
    EditText _sleepEditText = null;

    EditText _secondsWifiForcedSleepEditText = null;
    EditText _secondsDeepSleepEditText = null;

    Button _setThemAllToColorButton = null;

    ColorSet _currentColor = null;

    Handler _handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            //Log.d( "_debug", "entered handleMessage()" );
            //super.handleMessage(msg);
            updateUI();

            //! _server_javaMirror.update();
            updateDataFromServer();

            //Log.d( "_debug", "leaving handleMessage()" );
        };
    };

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    public String getWifiName(Context in_context) {
        WifiManager wifiManager = (WifiManager) in_context.getSystemService(Context.WIFI_SERVICE);
        String output = null;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
            output = wifiInfo.getSSID();
            output = output.substring(1, output.length()-1);
        }
        return output;
    }


    public void updateDataFromServer(){
        _numberOfClients_old = _numberOfClients;
        _numberOfClients = getNumberOfClientsFromCppServer();
        _serverRunning_old = _serverRunning;
        _serverRunning = getCppServerRunning();
    }


    @Override
    public void onAttach(Context context) {
        //Log.d("_debug", "entering ServerTab::onAttach()");
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        //Log.d("_debug", "entering ServerTab::onCreate()");
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        //Log.d("_debug", "entering ServerTab::onStart()");
        super.onStart();
        _updateUI_inLoop = true;
        start_updateUI_loop();
    }

    @Override
    public void onResume() {
        //Log.d("_debug", "entering ServerTab::onResume()");
        super.onResume();
        _handler.sendEmptyMessage(0);        //this is not absolutely necessary here, but it makes sure, that (after a swipe and back) the UI is in the old (running?) state immediately (without a small time where the gray "not-running" UI) can be seen
    }

    @Override
    public void onPause() {
        //Log.d("_debug", "entering ServerTab::onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        //Log.d("_debug", "entering ServerTab::onStop()");
        super.onStop();
        _updateUI_inLoop = false;
    }

    @Override
    public void onDestroy() {
        //Log.d("_debug", "entering ServerTab::onDestroy()");
        super.onDestroy();
    }



    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };

    public static void verifyStoragePermissions(Activity in_activity){
        if( ContextCompat.checkSelfPermission( in_activity, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED ) {
            Log.d("_debug", "Permission is not granted");
        }
    }

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ){
        //Log.d("_debug", "entering ServerTab::onCreateView()");
        super.onCreateView(inflater, container, savedInstanceState);
        View output = inflater.inflate( R.layout.server_tab, container, false );

        verifyStoragePermissions( getActivity() );

        _justCreatedView = true;

        _currentColor = new ColorSet(0, 0, 0, 0);

        _startServerButton = (Button) output.findViewById(R.id.startServer_button);
        if( getCppServerRunning() == false ){
            _startServerButton.setText("start server");
        }else{
            _startServerButton.setText("stop server");
        }
        _startServerButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startButtonPressed();
            }
        });
        _serverStatusTextView = (TextView) output.findViewById(R.id.serverStatus_textView);
        _contentLinearLayout = (LinearLayout) output.findViewById(R.id.content_LinearLayout);



        if( _serverRunning == false ){  //might be already set to true, after a tab-swipe
            _contentLinearLayout.setBackgroundColor(Color.GRAY);
        }
        _blinkButton = (Button) output.findViewById(R.id.makeThemBlink_button);
        _numberActiveClientsTextView = (TextView) output.findViewById( R.id.statusTab_numberOfClients_TextView);


        _sleepButton = (Button) output.findViewById( R.id.sleep_button );
        _sleepEditText = (EditText) output.findViewById(R.id.sleep_editText);
        _sleepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = _sleepEditText.getText().toString();
                int sleepForSeconds = Integer.parseInt( value );
                sleepAllClientsForSeconds( sleepForSeconds );
            }
        });

        _makeSignalsSwitch = (Switch) output.findViewById(R.id.makeSignals_switch);
        _makeSignalsSwitch.setChecked(true);
        _makeSignalsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                clientsShouldMakeSignals(isChecked);
            }

        });

        _setThemAllToColorButton = (Button)output.findViewById( R.id.setThemAllToColor_button );
        _setThemAllToColorButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick( View v ){
                showColorPicker();
            }
        });

        _secondsDeepSleepEditText = (EditText)output.findViewById(R.id.secondsDeepSleep_editText);
        _deepSleepSwitch = (Switch) output.findViewById(R.id.deepSleep_switch);
        _deepSleepSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isChecked){
                    int secondsToSleep = Integer.parseInt( _secondsDeepSleepEditText.getText().toString() );
                    setAllClientsToDeepSleep( secondsToSleep );
                }else{

                }
            }
        });

        _secondsWifiForcedSleepEditText = (EditText)output.findViewById(R.id.secondsWifiSleep_editText);
        _wifiForcedSleepSwitch = (Switch) output.findViewById(R.id.wifiForcedSleep_switch);
        _wifiForcedSleepSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if(isChecked){
                    int secondsToSleep = Integer.parseInt( _secondsWifiForcedSleepEditText.getText().toString() );
                    setAllClientsToWifiForcedSleepWithSeconds( secondsToSleep );
                }else{

                }
            }
        });

        _deepSleepSwitch = (Switch) output.findViewById(R.id.deepSleep_switch);

        //Log.d("_debug", "leaving Stauts_tab::onCreateView()");
        return output;
    }

    public void start_updateUI_loop(){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                while( _ui_updateRunnableActive ){  //to make absolutely sure, there is only one ui-update-thread
                    delay(300);
                    //Log.d("_debug", "ServerTab::start_updateUI_loop(): _ui_updateRunnableActive == true" );
                }

                _ui_updateRunnableActive = true;
                //delay(700 ); //this is just for a better feeling... we pretend the app must du a lot of stuff by introducing this delay here...
                updateDataFromServer();
                while( _updateUI_inLoop ){
                    //Log.d("_debug", "ServerTab::start_updateUI_loop(): calling _handler.sendEmptyMessage()");
                    _handler.sendEmptyMessage(0);
                    delay(1000 );   //wait 1 second
                }
                //Log.d("_debug", "ServerTab::start_updateUI_loop(): leaving ui-runnable!!!!!!!!!!!!!!!!!!!!!!!!!!");
                _ui_updateRunnableActive = false;
            }
        };
        if( _ui_updateRunnableActive == false ){    // after a veerry quick swipe to the non-neighbour-tab (e.g. Actions) and back it might be the case, that the runnable is still active (becuase it was sleeping). to avoid to start another one we check that here...
            //Log.d("_debug", "ServerTab::start_updateUI_loop(): starting new ui-runnable.....................");
            Thread thread = new Thread(runnable);
            thread.start();
        }else{
            //Log.d("_debug", "ServerTab::start_updateUI_loop(): not starting a new runnable, because _ui_updateRunnableActive == true");
        }
    }


    public void startButtonPressed(){
        //Log.d( "_debug", "entering ServerTab::startButtonPressed()");

        boolean serverRunning = getCppServerRunning();
        String currentWifiSSID = getWifiName( getContext() );
        if( currentWifiSSID != null ){

            if( currentWifiSSID.equals("multiversum") ){
                if( serverRunning == false ){
                    //Log.d("_debug", "serverRunning == false ... calling startCppServer()");
                    startCppServer();
                    if (_currentToast != null) {
                        _currentToast.cancel();
                    }
                    _currentToast = Toast.makeText(getActivity(), "starting Server...", Toast.LENGTH_SHORT);
                    _currentToast.show();
                    //updateDataFromServer();
                    //updateUI();

                } else {
                    //Log.d("_debug", "server already running ... calling stopCppServer()");
                    stopCppServer();
                }
            } else {
                if (_currentToast != null) {
                    _currentToast.cancel();
                }
                _currentToast = Toast.makeText(getActivity(), "fail! You are not connected to the 'multiversum'...", Toast.LENGTH_LONG);
                _currentToast.show();
            }

        }else{
             if (_currentToast != null) {
                 _currentToast.cancel();
            }
            _currentToast = Toast.makeText(getActivity(), "fail! Your wifi is not connected to anything...", Toast.LENGTH_LONG);
            _currentToast.show();
        }
        //Log.d( "_debug", "leaving ServerTab::startButtonPressed()");
    }


    public void updateUI(){
        //Log.d( "_debug", "entering ServerTab::updateUI()" );

        if( (_serverRunning && _serverRunning_old == false) || (_justCreatedView && _serverRunning) ) {
            // Log.d( "_debug", "\tserverRunning && serverRunning_old == false" );

            if (_justCreatedView) {
                _justCreatedView = false;
            }

            _serverStatusTextView.setText("Server running...");
            _startServerButton.setText("STOP SERVER");

            if( _currentToast != null ){
                _currentToast.cancel();
            }

            //adding the blink-button
            RelativeLayout.LayoutParams buttonDetails = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            buttonDetails.addRule(RelativeLayout.CENTER_VERTICAL);
            buttonDetails.addRule(RelativeLayout.CENTER_HORIZONTAL);

            _contentLinearLayout.setBackgroundColor(Color.WHITE);
            _blinkButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    makeThemBlinkWhite();
                }
            });

        }else if( _serverRunning == false && _serverRunning_old ){
            //Log.d("_debug", "    _server_javaMirror.getServerRunning() == false && _server_javaMirror.getServerRunning_old()");
            _serverStatusTextView.setText( "Server stopped..." );
            _startServerButton.setText( "START SERVER" );
            _contentLinearLayout.setBackgroundColor( Color.GRAY );
            _contentLinearLayout.removeView(_blinkButton);
        }

        int currentNumberOfClients = _numberOfClients;
        int oldNumberOfClients = _numberOfClients_old;

        if( currentNumberOfClients > oldNumberOfClients ){
            _numberActiveClientsTextView.setText( currentNumberOfClients + "" );
            if( _currentToast != null ){
                _currentToast.cancel();
            }
            _currentToast = Toast.makeText( getActivity(), "New Client(s) accepted...", Toast.LENGTH_SHORT);
            _currentToast.show();
        }else if( currentNumberOfClients < oldNumberOfClients ){
            _numberActiveClientsTextView.setText( currentNumberOfClients + "" );
            if( _currentToast != null ){
                _currentToast.cancel();
            }
            _currentToast = Toast.makeText( getActivity(), "Client(s) disconnected...", Toast.LENGTH_SHORT);
            _currentToast.show();
        }
        //Log.d( "_debug", "leaving ServerTab::updateUI()" );
        //_uiMutex.unlock();
    }

    private void delay( int in_delay ){
        synchronized (this) {   // why ever i need this...
            try {
                wait(in_delay);
            } catch (Exception exception) {
                Log.d("_debug", " ... wait(" + in_delay + ") throwed exception: " + exception);
            }
        }
    }

    public void showColorPicker(){

        // check: https://android-arsenal.com/details/1/1693
        ColorPickerDialogBuilder.with( getActivity() ).setTitle("Choose color").initialColor( Color.WHITE ).wheelType(ColorPickerView.WHEEL_TYPE.FLOWER).density(10).setOnColorSelectedListener(new OnColorSelectedListener(){
            @Override
            public void onColorSelected(int in_selectedColor) {
                String colorHexString = "" + ( Integer.toHexString(in_selectedColor) ).toString().substring(2).toUpperCase();       // i know, this is ugly... it's on my list...
                //Toast.makeText( getActivity(), "onColorSelected: " + colorHexString, Toast.LENGTH_SHORT).show();
                int color[] = getRGB( colorHexString );
                //Log.d("_debug", "R: " + color[0] + " G: " + color[1] + " B: " + color[2]);
                //doSomethingWithColor( color );
                updateCurrentColor_rgb( color[0], color[1], color[2] );
                setAllClientsToColor( _currentColor.red, _currentColor.green, _currentColor.blue, _currentColor.uv );
            }
        }).setPositiveButton("ok", new ColorPickerClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int in_selectedColor, Integer[] allColors) {
                //doSomethingWithColor(in_selectedColor);
            }
        }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){

            }
        }).build().show();
    }

    public void updateCurrentColor_rgb( int in_red, int in_green, int in_blue ){
        _currentColor.red = in_red;
        _currentColor.green = in_green;
        _currentColor.blue = in_blue;

        int percentageRed = (int) ( (  ((float)in_red)/((float)255.0)  )*100 + 0.5);
        int percentageGreen = (int) ( (  ((float)in_green)/((float)255.0)  )*100 + 0.5);
        int percentageBlue = (int) ( (  ((float)in_blue)/((float)255.0)  )*100 + 0.5);

        //_redValueTextView.setText(percentageRed + "%");
        //_greenValueTextView.setText(percentageGreen + "%");
        //_blueValueTextView.setText(percentageBlue + "%");
    }

    public void updateCurrentColor_uv( int in_uv ){
        _currentColor.uv = in_uv;

        int percentageUV = (int) ( (  ((float)in_uv)/((float)255.0)  )*100 + 0.5);

        //_uvValueTextView.setText(percentageUV + "%");
    }

    public static int[] getRGB(final String rgb){
        final int[] ret = new int[3];
        for (int i = 0; i < 3; i++)        {
            ret[i] = Integer.parseInt(rgb.substring(i * 2, i * 2 + 2), 16);
        }
        return ret;
    }

    public native void startCppServer();
    public native void stopCppServer();
    //public native boolean initServer();
    public native boolean getCppServerRunning();
    public native int getNumberOfClientsFromCppServer();
    public native void makeThemBlinkWhite();
    public native void sleepAllClientsForSeconds( int in_seconds );
    public native void clientsShouldMakeSignals( boolean in_shouldMakeSignals );
    public native void setAllClientsToWifiForcedSleepWithSeconds( int in_seconds );
    public native void setAllClientsToDeepSleep( int in_seconds );
    public native void setAllClientsToColor( int in_red, int in_green, int in_blue, int in_uv );
    //public native void setAppPath( String in_path );

}