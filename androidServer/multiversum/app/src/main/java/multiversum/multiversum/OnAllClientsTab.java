package multiversum.multiversum;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;


public class OnAllClientsTab extends android.support.v4.app.Fragment{

    boolean _justCreatedView = false;
    boolean _ui_updateRunnableActive = false;
    boolean _updateUI_inLoop = false;

    RelativeLayout _relativeLayout = null;

    boolean _serverRunning_old = false;
    boolean _serverRunning = false;

    Button _gameOfLifeButton = null;


    Handler _handler = new Handler(){
        @Override
        public void handleMessage( Message msg ) {
            //Log.d( "_debug", "entered BateriaSetupTab::handleMessage()" );
            super.handleMessage(msg);
            updateUI();

            updateDataFromServer();

            //Log.d( "_debug", "leaving BateriaSetupTab::handleMessage()" );
        };
    };

    public void updateDataFromServer(){
        _serverRunning_old = _serverRunning;
        _serverRunning = getCppServerRunning();
    }

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    @Override
    public void onAttach(Context context) {
        //Log.d("_debug", "entering OnMarkedClientsTab::onAttach()");
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        //Log.d("_debug", "entering OnMarkedClientsTab::onCreate()");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        //Log.d("_debug", "entering ActionTab::onStart()");

        super.onStart();

        _updateUI_inLoop = true;
        start_updateUI_loop();
    }

    @Override
    public void onResume() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onResume()");
        super.onResume();
        _handler.sendEmptyMessage(0);        //this is not absolutely necessary here, but it makes sure, that (after a swipe and back) the UI is in the old (running?) state immediately (without a small time where the gray "not-running" UI) can be seen
    }

    @Override
    public void onPause() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onStop()");
        super.onStop();
        _updateUI_inLoop = false;
    }

    @Override
    public void onDestroy() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onDestroy()");
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView( inflater, container, savedInstanceState );
        View output = inflater.inflate( R.layout.on_all_clients_tab, container, false );

        _gameOfLifeButton = (Button) output.findViewById( R.id.gameOfLife_button );
        _relativeLayout = (RelativeLayout) output.findViewById( R.id.onAllClients_relative_layout );

        _gameOfLifeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick( View v ){
                startGameOfLife();
            }
        });

        _relativeLayout.setBackgroundColor( Color.GRAY );

        return output;
    }


    public  void start_updateUI_loop(){

        Runnable runnable = new Runnable(){
            @Override
            public void run(){

                while( _ui_updateRunnableActive ){  //to make absolutely sure, there is only one ui-update-thread
                    delay(300);
                }

                _ui_updateRunnableActive = true;
                updateDataFromServer();
                while( _updateUI_inLoop ){
                    _handler.sendEmptyMessage(0);
                    delay(1000 );   //wait 1 second
                }
                _ui_updateRunnableActive = false;
            }
        };
        if( _ui_updateRunnableActive == false ){    // after a veerry quick swipe to the non-neighbour-tab (e.g. Actions) and back it might be the case, that the runnable is still active (becuase it was sleeping). to avoid to start another one we check that here...
            Thread thread = new Thread(runnable);
            thread.start();
        }else{
            //Log.d("_debug", "BateriaSetupTab::start_updateUI_loop(): not starting a new runnable, because _ui_updateRunnableActive == true");
        }
    }

    public void updateUI(){
        //Log.d( "_debug", "entering BateriaStatusTab::updateUI()" );
        if( (_serverRunning && _serverRunning_old == false) || (_justCreatedView && _serverRunning) ){
            _relativeLayout.setBackgroundColor(Color.WHITE);
            _justCreatedView = false;
        }else if( _serverRunning == false && _serverRunning_old || (_justCreatedView && _serverRunning == false) ){
            _relativeLayout.setBackgroundColor(Color.GRAY);
            _justCreatedView = false;
        }
    }

    private void delay( int in_delay ){
        synchronized (this) {   // why ever i need this...
            try {
                wait(in_delay);
            } catch (Exception exception) {
                Log.d("_debug", " ... wait(" + in_delay + ") throwed exception: " + exception);
            }
        }
    }

    public native void startGameOfLife();
    public native boolean getCppServerRunning();

}
