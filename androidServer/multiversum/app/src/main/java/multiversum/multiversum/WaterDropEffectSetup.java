package multiversum.multiversum;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

public class WaterDropEffectSetup extends AppCompatActivity {


    Button _pickColorButton = null;
    Switch _randomColorSwitch = null;
    SeekBar _uvSeekBar = null;
    TextView _redValueTextView = null;
    TextView _greenValueTextView = null;
    TextView _blueValueTextView = null;
    TextView _uvValueTextView = null;
    SeekBar _triggerThresholdSeekBar = null;
    SeekBar _speedSeekBar = null;
    CheckBox _stayColorCheckBox = null;
    SeekBar _attackSeekBar = null;
    SeekBar _releaseSeekBar = null;
    Button _cancleButton = null;
    Button _setButton = null;
    ColorSet _currentColor;
    int _row = -1;
    int _colomn = -1;
    String _chipID = "unknown";
    int _triggerThreshold = 0;
    int _speed = 1;
    int _attack = 1;
    int _release = 1;
    boolean _stayOnColor = false;
    boolean _randomColor = false;

    Toast _currentToast = null;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.water_drop_effect_setup);

        _pickColorButton = (Button) findViewById(R.id.waterDrop_pickColor_button);
        _randomColorSwitch = (Switch) findViewById(R.id.randomColor_switch);
        _uvSeekBar = (SeekBar) findViewById(R.id.waterDrop_uv_seekBar);
        _redValueTextView = (TextView) findViewById(R.id.red_value_textView);
        _greenValueTextView = (TextView) findViewById(R.id.green_value_textView);
        _blueValueTextView = (TextView) findViewById(R.id.blue_value_textView);
        _uvValueTextView = (TextView) findViewById(R.id.uv_value_textView);
        _speedSeekBar = (SeekBar) findViewById(R.id.speed_seekBar);
        _triggerThresholdSeekBar = (SeekBar) findViewById(R.id.triggerThreshold_seekBar);
        _stayColorCheckBox = (CheckBox) findViewById(R.id.stayColor_checkBox);
        _attackSeekBar = (SeekBar) findViewById(R.id.attack_seekBar);
        _releaseSeekBar = (SeekBar) findViewById(R.id.release_seekBar);
        _cancleButton = (Button)findViewById( R.id.cancle_button );
        _setButton = (Button)findViewById( R.id.set_button );


        final boolean createNew = getIntent().getBooleanExtra("new", true );   //

        _row = getIntent().getIntExtra("row", _row);        // whut a rotten interface... i have no idea, what the second argument is doing there... well i guess, it's the default value...
        _colomn = getIntent().getIntExtra("colomn", _colomn);
        if( _row == -1 && _colomn == -1 ){
            _chipID = getIntent().getStringExtra("chipID");
        }

        _currentColor = new ColorSet(0, 0, 0, 0);
        if( createNew ){
            // hm... nottn?
        }else{
            //if( _row != -1 && _colomn != -1 ){
            _currentColor = getEffectColorOnPosition(_row, _colomn);
            //}else{
            //    _currentColor = getEffectColorOnChip( _chipID );
            //}
        }


        _pickColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showColorPicker();
            }
        });

        if( createNew ){
            _randomColor = false;
        }else{
            //if( _row != -1 && _colomn != -1 ) {
            _randomColor = getEffectHasRandomColorOnPosition( _row, _colomn );
            //}else{
            //    _randomColor = getEffectHasRandomColorOnChip( _chipID );
            //}
        }

        _uvSeekBar.setMax(1023);
        if( createNew == false ){
            updateCurrentColor_uv( _currentColor.uv );
            _uvSeekBar.setProgress( _currentColor.uv );
        }
        _uvSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch( SeekBar seekBar ){
                int uvValue = _uvSeekBar.getProgress();
                updateCurrentColor_uv( uvValue );
            }
        });
        if( createNew == false ){
            if( _randomColor ){
                _pickColorButton.setVisibility(View.GONE);
                _uvSeekBar.setVisibility(View.GONE);
                _redValueTextView.setText("rndm");
                _greenValueTextView.setText("rndm");
                _blueValueTextView.setText("rndm");
                _uvValueTextView.setText("rndm");
            }else{
                _pickColorButton.setVisibility(View.VISIBLE);
                _uvSeekBar.setVisibility(View.VISIBLE);
                _redValueTextView.setText(_currentColor.red + "");
                _greenValueTextView.setText(_currentColor.green + "");
                _blueValueTextView.setText(_currentColor.blue + "");
                _uvValueTextView.setText(_currentColor.uv + "");
            }
        }

        _randomColorSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton in_buttonView, boolean in_isChecked) {
                _randomColor = in_isChecked;
                if( in_isChecked ){
                    _pickColorButton.setVisibility(View.GONE);
                    _uvSeekBar.setVisibility(View.GONE);
                    _redValueTextView.setText("rndm");
                    _greenValueTextView.setText("rndm");
                    _blueValueTextView.setText("rndm");
                    _uvValueTextView.setText("rndm");
                }else{
                    _pickColorButton.setVisibility(View.VISIBLE);
                    _uvSeekBar.setVisibility(View.VISIBLE);
                    _redValueTextView.setText(_currentColor.red + "");
                    _greenValueTextView.setText(_currentColor.green + "");
                    _blueValueTextView.setText(_currentColor.blue + "");
                    _uvValueTextView.setText(_currentColor.uv + "");
                }
            }
        });

        /*
        if( createNew ){
            _redValueTextView.setText("0 %");

            _greenValueTextView.setText("0 %");

            _blueValueTextView.setText("0 %");

            _uvValueTextView.setText("0 %");
        }
        */

        _triggerThresholdSeekBar.setMax(1023); // a pwm on the esp8266 can have a value from 0 to 1023
        if( createNew ){
            _triggerThresholdSeekBar.setProgress(0);
        }else{
            _triggerThreshold = getTriggerThresholdOnPosition( _row, _colomn );
            _triggerThresholdSeekBar.setProgress( _triggerThreshold );
        }
        _triggerThresholdSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar){
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar){
                if( createNew ){
                    _triggerThreshold = _triggerThresholdSeekBar.getProgress();
                }else{
                    _triggerThreshold = _triggerThresholdSeekBar.getProgress();
                    setTriggerThresholdOnEffectOnPosition( _row, _colomn, _triggerThreshold );
                }
            }
        });

        _speedSeekBar.setMax(9);   // we add later a '1', so it's between 1 and 10
        if( createNew ){
            _speedSeekBar.setProgress(0);
        }else{
            _speed = getEffectSpeedOnPosition( _row, _colomn );
            _speedSeekBar.setProgress( _speed - 1 );
        }
        _speedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int in_progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                _speed = _speedSeekBar.getProgress() + 1;  // + 1 because 0 would be unkewl, since we calculate the delay with 1/_speed
                Log.d("_debug", "_speed: " + _speed);
            }
        });



        if( createNew ){
            _stayOnColor = false;
        }else{
            _stayOnColor = getEffectStayOnColorOnPosition( _row, _colomn );
        }
        _stayColorCheckBox.setChecked( _stayOnColor );
        _stayColorCheckBox.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged( CompoundButton buttonView, boolean isChecked ){
                _stayOnColor = isChecked;
                if( isChecked ){
                    _releaseSeekBar.setVisibility(View.GONE);
                }else{
                    _releaseSeekBar.setVisibility(View.VISIBLE);
                }
            }
        });


        _attackSeekBar.setMax(128);
        if( createNew ){
            _attack = 0;
        }else{
            _attack = getEffectAttackOnPosition( _row, _colomn );
            _attackSeekBar.setProgress( _attack - 1 );
        }
        _attackSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int in_progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                _attack = _attackSeekBar.getProgress() + 1;
            }
        });

        _releaseSeekBar.setMax(128);
        if( createNew ){
            _release = 0;
        }else{
            _release = getEffectReleaseOnPosition( _row, _colomn );
            _releaseSeekBar.setProgress( _release - 1 );
        }
        _releaseSeekBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged( SeekBar seekBar, int in_progress, boolean fromUser ){
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                _release = _releaseSeekBar.getProgress() + 1;
            }
        });


        _cancleButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick( View v ){
                finish();
            }
        });


        _setButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                boolean success = false;
                if( _row == -1 && _colomn == -1 ){
                    success = setWaterDropEffectOnChip( _chipID, _currentColor.red, _currentColor.green, _currentColor.blue, _currentColor.uv, _triggerThreshold, _speed, _stayOnColor, _attack, _release, _randomColor );
                }else{
                    if( createNew ){
                        success = setWaterDropEffectOnPosition( _row, _colomn, _currentColor.red, _currentColor.green, _currentColor.blue, _currentColor.uv, _triggerThreshold, _speed, _stayOnColor, _attack, _release, _randomColor );
                    }else{
                        success = editWaterDropEffectOnPosition( _row, _colomn, _currentColor.red, _currentColor.green, _currentColor.blue, _currentColor.uv, _speed, _stayOnColor, _attack, _release, _randomColor );
                    }
                }
                if( success ){
                    if( _currentToast != null ){
                        _currentToast.cancel();
                    }
                    if( createNew ){
                        _currentToast = Toast.makeText(getApplicationContext(), "successfully set new water-drop-effect", Toast.LENGTH_LONG);
                    }else{
                        _currentToast = Toast.makeText(getApplicationContext(), "successfully edited water-drop-effect", Toast.LENGTH_LONG);
                    }
                    _currentToast.show();
                    finish();
                }else{
                    if( _currentToast != null ){
                        _currentToast.cancel();
                    }
                    _currentToast = Toast.makeText(getApplicationContext(), "failed to set water-drop-effect!!!", Toast.LENGTH_LONG);
                    _currentToast.show();
                }
            }
        });

        updateCurrentColor_rgb( _currentColor.red, _currentColor.green, _currentColor.blue );
        updateCurrentColor_uv( _currentColor.uv );

    }

    private ColorSet getEffectColorOnPosition( int in_row, int in_colomn ){
        ColorSet output = new ColorSet();
        output.red = getRedValueOfEffectOnPosition( in_row, in_colomn );
        output.blue = getBlueValueOfEffectOnPosition( in_row, in_colomn );
        output.green = getGreenValueOfEffectOnPosition( in_row, in_colomn );
        output.uv = getUVValueOfEffectOnPosition( in_row, in_colomn );
        return output;
    }

    private void showColorPicker(){
        // check: https://android-arsenal.com/details/1/1693
        ColorPickerDialogBuilder.with( this ).setTitle("Choose color").initialColor( Color.WHITE ).wheelType(ColorPickerView.WHEEL_TYPE.FLOWER).density(10).setOnColorSelectedListener(new OnColorSelectedListener(){
            @Override
            public void onColorSelected(int in_selectedColor) {
            }
        }).setPositiveButton("ok", new ColorPickerClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int in_selectedColor, Integer[] allColors) {
                String colorHexString = "" + ( Integer.toHexString(in_selectedColor) ).toString().substring(2).toUpperCase();
                int color[] = getRGB( colorHexString );
                updateCurrentColor_rgb( color[0], color[1], color[2] );
            }
        })
        /*
        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){
            }
        })
        */
        .build().show();
    }

    private void updateCurrentColor_rgb( int in_red, int in_green, int in_blue ){

        _currentColor.red = in_red;
        _currentColor.green = in_green;
        _currentColor.blue = in_blue;

        int percentageRed = (int) ( (  ((float)in_red)/((float)255.0)  )*100 + 0.5 );
        int percentageGreen = (int) ( (  ((float)in_green)/((float)255.0)  )*100 + 0.5 );
        int percentageBlue = (int) ( (  ((float)in_blue)/((float)255.0)  )*100 + 0.5 );

        _redValueTextView.setText(percentageRed + "%");
        _greenValueTextView.setText(percentageGreen + "%");
        _blueValueTextView.setText(percentageBlue + "%");
    }

    public void updateCurrentColor_uv( int in_uv ){
        _currentColor.uv = in_uv;

        int percentageUV = (int) ( (  ((float)in_uv)/((float)1023.0)  )*100 + 0.5);

        _uvValueTextView.setText(percentageUV + "%");
    }

    private static int[] getRGB(final String in_rgb){
        final int[] ret = new int[3];
        for (int i = 0; i < 3; i++)        {
            ret[i] = Integer.parseInt( in_rgb.substring(i * 2, i * 2 + 2), 16 );
        }
        return ret;
    }



    public native boolean setWaterDropEffectOnPosition( int in_row, int in_colomn, int in_red, int in_green, int in_blue, int in_uv, int in_triggerThreshold, int in_speed, boolean in_stayOnColor, int in_attack, int in_release, boolean in_randomColor );
    public native boolean editWaterDropEffectOnPosition( int in_row, int in_colomn, int in_red, int in_green, int in_blue, int in_uv, int in_speed, boolean in_stayOnColor, int in_attack, int in_release, boolean in_randomColor );
    public native boolean setWaterDropEffectOnChip( String in_chipID, int in_red, int in_green, int in_blue, int in_uv, int in_triggerThreshold, int in_speed, boolean in_stayOnColor, int in_attack, int in_release, boolean in_randomColor );
    public native boolean getEffectHasRandomColorOnPosition( int in_row, int in_colomn );
    public native int getRedValueOfEffectOnPosition( int in_row, int in_colomn );
    public native int getGreenValueOfEffectOnPosition( int in_row, int in_colomn );
    public native int getBlueValueOfEffectOnPosition( int in_row, int in_colomn );
    public native int getUVValueOfEffectOnPosition( int in_row, int in_colomn );
    //public native boolean bateriaExists();
    public native int getEffectSpeedOnPosition( int in_row, int in_colomn );
    public native boolean getEffectStayOnColorOnPosition( int in_row, int in_colomn );
    public native int getEffectAttackOnPosition( int in_row, int in_colomn );
    public native int getEffectReleaseOnPosition( int in_row, int in_colomn );
    public native void setTriggerThresholdOnEffectOnPosition( int in_row, int in_colomn, int in_newTriggerThreshold );
    public native int getTriggerThresholdOnPosition( int in_row, int in_colomn );
}
