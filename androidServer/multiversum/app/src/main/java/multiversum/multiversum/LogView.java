package multiversum.multiversum;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class LogView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_view);

        /*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        */

        showData();


    }

    public void showData(){
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String completeLog = extras.getString("log");
            TextView logContentTextView = (TextView)findViewById( R.id.logContent_textView );
            logContentTextView.setText( completeLog );
        }
    }

}
