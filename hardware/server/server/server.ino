
#include <vector>
#include <SPI.h>
#include <ESP8266WiFi.h>


#define TRYES_TO_CONNECT 1
#define DEBUG false
#define NETWORK_NAME "multiversum"
#define NETWORK_KEY "4815162342"
#define USE_DISPLAY true
#define BUTTON_0 14
#define BUTTON_1 12
#define BUTTON_2 5
#define BUTTON_3 4
#define BUTTON_4 13

#if USE_DISPLAY
#define SPI_SS 15
#define SPI_CLK 14
#define SPI_MOSI 13
#endif

#define MIN_TIME_BETWEEN_PINGS 1500 //milliseconds

using namespace std;

typedef struct {
  WiFiClient wifiClient;
  vector< String > outgoingMessageStack;    // need to do it this way, because the chip crashes if i try to write a socket after in an interrupt
  String signalStrength = "";
} client_struct;


typedef struct {
  int red = 0;
  int green = 0;
  int blue = 0;
  int uv = 0;
} colorSet;


WiFiServer server(4223);
vector< client_struct > allClients;
bool connectedToWifi = false;
byte WiFistatus = WL_DISCONNECTED;
int TryesToConnect = 0;

String messageBuffer = "";
int numberOfCompleteMessagesInMessgeBuffer = 0;

int loopCounter = 0;
vector< int > indexesOfClientsToDelete;

unsigned long timeLastPingSent = 0;



void display(int in_input) {

    //Serial.print("entering display() ; in_input:  ");
    //Serial.println(in_input);
  
    #if USE_DISPLAY
    digitalWrite(SPI_SS, LOW); //CS enable
    SPI.transfer( in_input / 1000 );
    in_input %= 1000;
    SPI.transfer( in_input / 100 );
    in_input %= 100;
    SPI.transfer( in_input / 10 );
    in_input %= 10;
    SPI.transfer( in_input );
    digitalWrite(SPI_SS, HIGH); //CS disable
    #endif
  
    //Serial.println("leaving display()");

}

void setup() {
  
    delay(5000);
    Serial.begin(9600);
    Serial.println("Serial initialized. this is a server");
    delay(5000);
    #if USE_DISPLAY
    pinMode( SPI_MOSI, OUTPUT );
    pinMode( SPI_SS, OUTPUT );
    pinMode( SPI_CLK, OUTPUT );
    digitalWrite( SPI_SS, HIGH );
    SPI.begin();
    SPI.setClockDivider( SPI_CLOCK_DIV128 );// slowing down the master-speed a bit
    #endif
    display(8888);
  
    Serial.println("calling WiFi.disconnect()");
    WiFi.disconnect();
    delay(1000);
  
    server.begin();
  
    pinMode(BUTTON_0, INPUT);
    attachInterrupt( digitalPinToInterrupt( BUTTON_0 ), handleInterrupt_button_0, RISING );
    pinMode(BUTTON_1, INPUT);
    attachInterrupt( digitalPinToInterrupt( BUTTON_1 ), handleInterrupt_button_1, RISING );
    pinMode(BUTTON_2, INPUT);
    attachInterrupt( digitalPinToInterrupt( BUTTON_2 ), handleInterrupt_button_2, RISING );
    pinMode(BUTTON_3, INPUT);
    attachInterrupt( digitalPinToInterrupt( BUTTON_3 ), handleInterrupt_button_3, RISING );
    pinMode(BUTTON_4, INPUT);
    attachInterrupt( digitalPinToInterrupt( BUTTON_4 ), handleInterrupt_button_4, RISING );
  
  
    Serial.println("leaving setup()");

}


void handleInterrupt_button_0(){
    #if DEBUG
    Serial.println("entering handleInterrupt_button_0 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    #endif
  
    colorSet greenColorSet;
    greenColorSet.green = 255;
  
    fadeToColorInSteps( greenColorSet, 1 );
  
    #if DEBUG
    Serial.println("leaving handleInterrupt_button_0");
    #endif
}


void handleInterrupt_button_1(){
    #if DEBUG
    Serial.println("entering handleInterrupt_button_1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    #endif
  
    //Serial.println("calling interrupts()");
    //interrupts();
  
    colorSet redColorSet;
    redColorSet.red = 255;
  
    fadeToColorInSteps( redColorSet, 1 );
  
    #if DEBUG
    Serial.println("leaving handleInterrupt_button_1");
    #endif
}




void handleInterrupt_button_2(){
    #if DEBUG
    Serial.println("entering handleInterrupt_button_2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    #endif


    colorSet blueColorSet;
    blueColorSet.blue = 255;
    //colorSet redColorSet;
    //redColorSet.red = 255;
    fadeToColorInSteps(blueColorSet, 10);
  
    #if DEBUG
    Serial.println("leaving handleInterrupt_button_2");
    #endif
}


void handleInterrupt_button_3(){
    #if DEBUG
    Serial.println("entering handleInterrupt_button_3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    #endif
  
    colorSet color;
    color.blue = 255;
    color.red = 255;
    color.green = 255;
    color.uv = 255;
  
    blinkInColorForMilliseconds( color, 300 );
  
    #if DEBUG
    Serial.println("leaving handleInterrupt_button_3");
    #endif
}


void handleInterrupt_button_4(){
    #if DEBUG
    Serial.println("entering handleInterrupt_button_4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    #endif
  
    String toSendMessage = "@fadeSeries4AllPixels;";

    // fading slowly to green...
    colorSet red;
    red.red = 255;
    addFadeToFadeSeriesString( &toSendMessage, red, 32, 1000 );


    //then to blue, a little bit faster...
    colorSet blue;
    blue.blue = 255;
    addFadeToFadeSeriesString( &toSendMessage, blue, 20, 2000 );

    //then blinking three times in white...
    colorSet white;
    white.red = 255;
    white.blue = 255;
    white.green = 255;
    white.uv = 0;
    colorSet off;
    addFadeToFadeSeriesString( &toSendMessage, white, 1, 300 );
    addFadeToFadeSeriesString( &toSendMessage, off, 1, 300 );
    addFadeToFadeSeriesString( &toSendMessage, white, 1, 300 );
    addFadeToFadeSeriesString( &toSendMessage, off, 1, 300 );
    addFadeToFadeSeriesString( &toSendMessage, white, 1, 300 );
    addFadeToFadeSeriesString( &toSendMessage, off, 1, 2000 );

    toSendMessage += "#";
    addMessageToAllClientsOutgoingMessageStack( toSendMessage );
  
    #if DEBUG
    Serial.println("leaving handleInterrupt_button_3");
    #endif
}

void blinkInColorForMilliseconds( colorSet in_colorSet, int in_milliseconds ){
    #if DEBUG
    Serial.println("entering blinkInColorForMilliseconds()");
    #endif

    
    
    String toSendMessage = "@fadeSeries4AllPixels;";
    colorSet LEDoff_color;
    addFadeToFadeSeriesString( &toSendMessage, in_colorSet, 1, in_milliseconds );
    addFadeToFadeSeriesString( &toSendMessage, LEDoff_color, 1, 0 );
    toSendMessage += "#";
    
    addMessageToAllClientsOutgoingMessageStack( toSendMessage );
    #if DEBUG
    Serial.println("leaving blinkInColorForMilliseconds()");
    #endif
}


void addFadeToFadeSeriesString( String* in_eventString, colorSet in_color, int in_steps, int in_holdMilliseconds ){

    // a event in a sequence of fades, which will transmitted in this form: 255,0,0,0-1-300_
    // this is an example for a blinkin in red (we switch to this color in 1 step, and hold that for 300 milliseconds
    // but thats only half of it...
    // after that it only makes sense to change to another color, like 0,0,0,0 (led is off)
    // so this two fades (packed in one event) look like this: 255,0,0,0-1-300_0,0,0,0-1-1337_
    // the last number (1337) does not matter in this case

    *in_eventString += serializeColorSet( in_color );
    *in_eventString += '-';
    *in_eventString += String( in_steps );
    *in_eventString += '-';
    *in_eventString += String( in_holdMilliseconds );
    *in_eventString += '_';
    
    
}


void fadeToColorInSteps( colorSet in_colorSet_To, int in_steps ){

    #if DEBUG
    Serial.println("entering fadeBetweenColorInSteps()");
    #endif

    
    String toSendMessage = "@fadeSeries4AllPixels;";
    addFadeToFadeSeriesString( &toSendMessage, in_colorSet_To, in_steps, 0 );
    toSendMessage += "#";
    
    addMessageToAllClientsOutgoingMessageStack( toSendMessage );    // why are we not sending it directly but put it on a MessageStack, which will be processed later?? reason: the fadeToColorInSteps()-function will be called in an interrupt, and in an interrupt it seems not to be possible to write the socket... that's why it would make more sense to poll the button in the main loop() and not to work with interrupts here...

    #if DEBUG
    Serial.println("leaving fadeBetweenColorInSteps()");
    #endif
    
}


String serializeColorSet( colorSet in_colorSet ){
    String output = "";

    output += String( in_colorSet.red );
    output += ",";
    output += String( in_colorSet.green );
    output += ",";
    output += String( in_colorSet.blue );
    output += ",";
    output += String( in_colorSet.uv );
    
    return output;
}

/*
void setAllClientsToColorSet( colorSet in_colorSet ) {
  
    #if DEBUG
    Serial.println("entering setAllClientsToColorSet()");
    #endif
  
    String toSendString = "@setAllColor;";
    //Serial.print("in_colorSet.red: ");
    //Serial.println(in_colorSet.red);
    String redString = String( in_colorSet.red );
    //Serial.print("redString: ");
    //Serial.println(redString);
    toSendString += redString;
    toSendString += ",";
    String greenString = String( in_colorSet.green );
    //Serial.print("greenString: ");
    //Serial.println(greenString);
    toSendString += greenString;
    toSendString += ",";
    String blueString = String( in_colorSet.blue );
    //Serial.print("blueString: ");
    //Serial.println(blueString);
    toSendString += blueString;
    toSendString += ",";
    String whiteString = String( in_colorSet.white );
    toSendString += whiteString;
    toSendString += '#';
  
    //Serial.print("toSendString: ");
    //Serial.println(toSendString);
    for ( int index = 0; index < allClients.size(); index++ ) {
        //writeClient( index, "@pong;-#" );
    
        //!writeClient( index, toSendString );
        addMessageToClientsOutgoingMessageStack(index, toSendString);
    
    }
    #if DEBUG
    Serial.println("leaving setAllClientsToColorSet()");
    #endif
}

*/

void addMessageToAllClientsOutgoingMessageStack( String in_message ){

    for( int index = 0; index < allClients.size(); index++ ){
        addMessageToClientsOutgoingMessageStack( index, in_message );
    }

    
}


void addMessageToClientsOutgoingMessageStack( int in_clientIndex, String in_message ) {

    ( allClients[in_clientIndex] ).outgoingMessageStack.push_back( in_message );

}


void processOutgoingMessageStacks() {
    #if DEBUG
    Serial.println("entering processOutgoingMessageStack()");
    #endif
  
    for ( int clientIndex = 0; clientIndex < allClients.size(); clientIndex++ ) {
        #if DEBUG
        Serial.print("cientIndex: ");
        Serial.println(clientIndex);
        #endif
        for ( int messageIndex = 0; messageIndex < allClients[clientIndex].outgoingMessageStack.size(); messageIndex++ ) {
          #if DEBUG
          Serial.print("messageIndex: ");
          Serial.println(messageIndex);
          #endif
          writeClient( clientIndex, allClients[clientIndex].outgoingMessageStack[messageIndex] );
        }
        allClients[clientIndex].outgoingMessageStack.clear();
    }
    #if DEBUG
    Serial.println("leaving processOutgoingMessageStack()");
    #endif
}



void reset(  ) {
    Serial.println("entering reset()");
    delay(2000);
  
    /*
      digitalWrite( 12, LOW );
      delay(1000);
    */
  
    ESP.reset();
    delay(2000);
    Serial.println("leaving reset()");
}


void connectToWifi() {
  
    #if DEBUG
    Serial.println("entering connectToWifi()");
    Serial.print("WiFi.status() = ");
    Serial.println( WiFi.status() );
    #endif
  
    connectedToWifi = false;
  
  
    WiFi.config( IPAddress(192, 168, 43, 2), IPAddress(192, 168, 43, 1), IPAddress(255, 255, 255, 0) /*, IPAddress(192,168,43,1)*/ );
    // look at:
    // https://wolf-u.li/5585/konfiguration-einer-statischen-ip-fuer-den-esp8266-und-arduino/
    // and:
    // https://forum.arduino.cc/index.php?topic=434475.0
  
    if ( WiFi.status() !=  WL_CONNECTED ) { //for some spooky reason, the wifi uses to connect BEFOR executing the following lines.. in that case, we don't need to re-try...
        //for( int j = 0; j < 1 && WiFistatus == WL_DISCONNECTED; j++ ){
        for ( int i = 0; i < TRYES_TO_CONNECT && WiFistatus == WL_DISCONNECTED; i++ ) {
      
      
            #if DEBUG
            Serial.println("entering mobile");
            Serial.print("trying to connect to wifi '");
            Serial.print( NETWORK_NAME );
            Serial.print( "' and password '");
            Serial.print( NETWORK_KEY );
            Serial.println( "' ...");
            #endif
            WiFistatus = WiFi.begin(NETWORK_NAME, NETWORK_KEY);
            //WiFistatus = WiFi.begin(NETWORK_NAME);
      
            if ( WiFistatus == 3 ) {
              connectedToWifi = true;
            }
      
            #if DEBUG
            Serial.print( "WiFistatus: " );
            Serial.println( WiFistatus );
            #endif
            delay(3000);                      // wait 3 seconds for connection:
        }
      //}
    } else {
        #if DEBUG
        Serial.println("already connected!");
        #endif
    }
    #if DEBUG
    Serial.print("connected with: ");
    Serial.println( WiFi.SSID() );
    Serial.print("IP: ");
    Serial.println( WiFi.localIP() );
    Serial.print("gateway: " );
    Serial.println(WiFi.gatewayIP());
    #endif
  
    TryesToConnect++;
    #if DEBUG
    Serial.print("TryesToConnect: ");
    Serial.println(TryesToConnect);
    #endif
    if ( TryesToConnect >= 5 ) {
        TryesToConnect = 0;
        #if DEBUG
        Serial.println("calling reset()");
        #endif
        reset();
    }
  
    Serial.print( "WiFi.localIP(): " );
    Serial.println( WiFi.localIP() );
  

}


/*
  void connectToWifi(){

    Serial.print("enteirng connectToWifi() .... WiFistatus: ");
    Serial.println( WiFistatus );

    Serial.println("trying to connect...");

    WiFistatus = WiFi.begin( NETWORK_NAME, NETWORK_KEY );

    Serial.print("WiFistatus: " );
    Serial.println(WiFistatus);

    //delay(50);


    // i know, this is a dirty hack. but i had the problem, that the display wouldn't reconnect to the detecter, once it has lost the connection.
    tryesToConnect++;
    Serial.print("tryesToConnect: ");
    Serial.println(tryesToConnect);
    if( tryesToConnect == 20 ){
        Serial.println("calling reset()");
        reset();
    }


    Serial.println("leaving connectToWifi()");

  }

*/

void writeAllClients( String in_toSendMessage ){

    for( int index = 0; index < allClients.size(); index++ ){
        writeClient( index, in_toSendMessage );
    }
    
}


void writeClient( int in_clientIndex, String in_toSendMessage ) {
  
    #if DEBUG
    Serial.print("trying to write client[");
    Serial.print( in_clientIndex );
    Serial.print("] ... in_toSendMessage: ");
    Serial.print(in_toSendMessage);
    Serial.print(" ... ");
    #endif
  
    int bytesWritten = allClients[in_clientIndex].wifiClient.print( in_toSendMessage );
    #if DEBUG
    Serial.print("done writing. bytesWritten: ");
    Serial.println(bytesWritten);
    #endif

    
    if ( bytesWritten == 0 ) {
        #if DEBUG
        Serial.print("client[");
        Serial.print( in_clientIndex );
        Serial.println("] seems offline ... removing it from array");
        #endif
        //allClients.erase( allClients.begin() + in_clientIndex );
        indexesOfClientsToDelete.push_back( in_clientIndex );
    }
    
}

void eraseClients() {
    #if DEBUG
    Serial.println("entering eraseClients()");
    #endif
    for ( int index = 0; index < indexesOfClientsToDelete.size(); index++ ) {
        #if DEBUG
        Serial.print( "deleting client[");
        Serial.print(index);
        Serial.print("] ... ");
        #endif
        allClients.erase( allClients.begin() + indexesOfClientsToDelete[index] );
        #if DEBUG
        Serial.println("done!");
        #endif
        //delay(5000);
    }
    indexesOfClientsToDelete.clear();
    #if DEBUG
    Serial.println("leaving eraseClients()");
    #endif
}


void sendPingToAllClients() {

  //Serial.println("sending ping to all clients now!");
  for ( int index = 0; index < allClients.size(); index++ ) {
    //Serial.print("trying to write Client[");
    //Serial.print(index);
    //Serial.println("]");
    writeClient( index , "@ping;-#" );
    //Serial.println("done writing");

  }
  timeLastPingSent = millis();

}


void handlePing( String in_strengthInformation, int in_clientIndex ) {
    allClients[in_clientIndex].signalStrength = in_strengthInformation;
    #if DEBUG
    Serial.print("allClients[");
    Serial.print(in_clientIndex);
    Serial.print("].signalStrength: ");
    Serial.println(allClients[in_clientIndex].signalStrength);
    #endif
}


void processButtonPressed( int in_clientIndex, const String* in_commandContent ){

    Serial.println("entering processButtonPressed()");

    if( *in_commandContent == "positiveEdge" ){
        Serial.println("button0 positive edge. nothing set yet... ");
    }else if( *in_commandContent == "negativeEdge" ){
        Serial.println("button0 negative edge. nothing set yet... ");
    }else{
        Serial.print("error ... processButtonPressed(): in_commandContent: ");
        Serial.println(*in_commandContent);
    }
    Serial.println("leaving processButtonPressed()");
}

void processCommand( String in_command, int in_clientIndex ) {

    #if DEBUG
    Serial.print( "entering processCommand() ... in_command: " );
    Serial.println( in_command );
    #endif


    String commandType = "";
    bool commandTypeComplete = false;
    String commandContent = "";

    for( int index = 0; index < in_command.length(); index++ ){
        char symbol = in_command.charAt( index );
        if( symbol == ';' ){
            commandTypeComplete = true;
            index++;
            symbol = in_command.charAt( index );
        }

        if( commandTypeComplete == false ){
            commandType += symbol;
        }else{
            commandContent += symbol;
            
        }
    }
  
    if( commandType == "ping" ) {
        handlePing( commandContent, in_clientIndex );

    }else if( commandType == "button0" ){
        processButtonPressed( in_clientIndex, &commandContent );
    }else{
        #if DEBUG
        Serial.print("unknown command ... in_command: ");
        Serial.println( in_command );
        #endif
    }
    //! more comming late...
  
  
    //allClients[ in_clientIndex ].lastSeen = millis();


}


void readFromSocketAndExecute( int in_clientIndex ) {

    #if DEBUG
    Serial.print("entering readFromSocketAndExecute() for client ");
    Serial.println(in_clientIndex);
    #endif
  
    char symbol = 0;
    do {
        symbol = allClients[in_clientIndex].wifiClient.read();
        if ( symbol != 255 ) {
            messageBuffer += symbol;
        }
        if ( symbol == '#' ) {
            numberOfCompleteMessagesInMessgeBuffer++;
        }
    } while ( (int)symbol != 255 );
    
    for ( int index = 0; index < numberOfCompleteMessagesInMessgeBuffer; index++ ) {

        String receivedCommand = "";
        bool complete = false;
        while ( complete == false ) {
            symbol = messageBuffer.charAt( 0 );
            if ( symbol == '#' ) {
              complete = true;
            }
            messageBuffer.remove( 0, 1 );
            if ( symbol != '#' && symbol != '@' ) {
              receivedCommand += symbol;
            }
        };
        processCommand( receivedCommand, in_clientIndex );
    }
    numberOfCompleteMessagesInMessgeBuffer = 0;

}


/*
  void checkAllClientsForTimeout(){

    Serial.println("entering checkAllClientsForTimeout()");


    unsigned long currentTime = millis();
    for( int index = 0; index < allClients.size(); index++ ){


        unsigned long timeDelta = currentTime - allClients[ index ].lastSeen;
        Serial.print("allClients[");
        Serial.print(index);
        Serial.print("]: timeDelta: " );
        Serial.println(timeDelta);
        if( timeDelta > 3000 ){
            Serial.print("removing Client with index ");
            Serial.print( index );
            Serial.println(" because of timeout (timeDelta > 3000)");
            allClients.erase( allClients.begin() + index );
        }

    }

    Serial.println("leaving checkAllClientsForTimeout()");

  }
*/

void loop() {
  
    #if DEBUG
    delay( 500 );
    #endif
  
    if ( WiFi.localIP() == (0, 0, 0, 0) ) {
        connectToWifi();
    }
  

    #if DEBUG
    if( loopCounter % 4 == 0 ){
    #else
    if( loopCounter % 32 == 0 ){
    #endif


      
        #if DEBUG
        Serial.print("checking for new clients (loopCounter=");
        Serial.print(loopCounter);
        Serial.println(")");
        #endif
        WiFiClient newClient;
        if ( newClient = server.available() ) { // listen for incoming clients
            #if DEBUG
            Serial.println("client = server.available() == true ");
            #endif
            client_struct temp_newClient_struct;
            temp_newClient_struct.wifiClient = newClient;
            allClients.push_back( temp_newClient_struct );
            delay(100);
        } else {
            #if DEBUG
            Serial.println("client = server.available() == false ");
            #endif
        }
    }
  
    #if DEBUG
    Serial.print("allClients.size() == ");
    Serial.println(allClients.size());
    #endif
    if ( allClients.size() > 0 ) {
        for ( int index = 0; index < allClients.size(); index++ ) {
            readFromSocketAndExecute( index );
        }

        unsigned long currentTime = millis();
        if( currentTime - timeLastPingSent > MIN_TIME_BETWEEN_PINGS ){
            sendPingToAllClients();
        }else{
            #if DEBUG
            Serial.print("to early to send next ping. currentTime - timeLastPingSent: ");
            Serial.println(currentTime - timeLastPingSent);
            #endif
        }
        
        eraseClients();
    
        processOutgoingMessageStacks();
    }
  
    loopCounter++;
}
