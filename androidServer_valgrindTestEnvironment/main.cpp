// this is a testszenario, for this situation:
// creating a bateria: 1 row, 5 colomns
// assigning 1 client to position 0;0
// assigning a water dropDropEffect to this position
// assigning the rest of the clients until position 0;4 (that makes 5 clients)
//
// that makes a good szenario for testing the effect






#include <iostream>

#include <sys/socket.h>     //        = needed for the socket and server stuff
#include <netinet/in.h>     //
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cstring>
#include <string>

#include "native-lib.cpp"

using namespace std;

int main(){
    cout << "calling initServer()" << endl;
    initServer( "/home/matze/multiversum/androidServer_valgrindTestEnvironment" );
    if( getCppServerRunning() ){
        cout << "cpp server is running" << endl;
    }else{
        cout << "cpp server not running" << endl;
    }

    int numberOfClients = getNumberOfClientsFromCppServer();
    cout << "numberOfClients: " << numberOfClients << endl;
    
    cout << "calling collectClientsInformation()" << endl;
    collectClientsInformation();
    int totalNumberOfClients = getTotalNumberOfClients();
    cout << "totalNumberOfClients: " << totalNumberOfClients << endl;

    if( getCppServerRunning() ){
        cout << "cpp server is running" << endl;
    }else{
        cout << "cpp server not running" << endl;
    }

    bool hasChanged = getSomeClientAssignmentHasChanged();
    cout << "hasChanged: " << hasChanged << endl;

    cout << "calling startCppServer()" << endl;
    startCppServer();
    
    /* worked
    cout << endl;
    cout << "press any key to get snapshot-information" << endl;
    cout << endl;
    getchar();
    collectClientsInformation();
    getChipIDfromSnapshotOnPosition( 0 );
    getVersionfromSnapshotOnPosition( 0 );
    getSignalStrengthfromSnapshotOnPosition( 0 );
    getLastSeenfromSnapshotOnPosition( 0 );
    getInstrumentFromSnapshotOnPosition( 0 );
    getSomeClientAssignmentHasChanged(  );
    getIpAdressFromSnapshotOnPosition( 0 );
    getLastStateFromSnapshotOnPosition( 0 );
    */
   
    cout << endl;
    cout << "press any key to create bateria" << endl;
    cout << endl;
    getchar();
    cout << "creating bateria" << endl;
    initBateria(1,5);

    /* worked
    bool somethingHasChanged = getSomeClientAssignmentHasChanged();
    getInstrumentOnPosition( 0, 0 );
    */




    for( size_t index = 0; index < 5; index++ ){
        cout << endl;
        cout << "press any key to call learnPosition(): row 0 ; colomn " << index << endl;
        cout << endl;
        getchar();
        learnPosition( 0, index );

        cout << "press button on client and then: press any key to set instrument caixa on client" << endl;
        getchar();
        /* worked
        somethingHasChanged = getSomeClientAssignmentHasChanged();
        */
        removeFromIsMarkedForActions( 0, index );
        blinkOnPositionWithColor( 0, index, 255, 255, 255, 0 );
        setInstrumentOnPosition( "caixa", 0, index );

        if( index == 0 /*|| index == 3*/ ){

            cout << endl;
            cout << "press any key to set water drop effect" << endl;
            cout << endl;
            getchar();
            print("calling setWaterDropEffectOnPosition() !!!!!!!!!!!!!!!!!!");
            bool success = false;
            if( index == 0 ){
                success = setWaterDropEffectOnPosition(0, index, 255, 0, 0, 0, 136, 3, false, 12, 15, false);
            }else if( index == 3 ){
                success = setWaterDropEffectOnPosition(0, index, 0, 255, 0, 0, 136, 3, false, 12, 64, false);
            }
            if( success ){
                cout << "successfully created WaterDropEffect" << endl;
            }else{
                cout << "failed creating WaterDropEffect" << endl;
            }

        }



    }

    /*
    cout << endl;
    cout << "press any key to call learnPosition(): row 0 ; colomn 0" << endl;
    cout << endl;
    getchar();
    learnPosition( 0, 0 );

    cout << "press button on client and then: press any key to set instrument caixa on client" << endl;
    getchar();

    removeFromIsMarkedForActions( 0, 0 );
    blinkOnPositionWithColor( 0, 0, 255, 255, 255, 0 );
    setInstrumentOnPosition( "caixa", 0, 0 );
    */

    




    while(true){
        getchar();
        for( size_t index = 0; index < 5; index++ ){
            blinkOnPositionWithColor( 0, index, 255, 255, 255, 0 );
        }
        cout << "blink!" << endl;
        //usleep(5000000);


    }


    cout << "leaving main" << endl;
    return 0;
}
